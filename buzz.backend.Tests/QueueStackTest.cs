﻿using buzz.backend.Controllers;
using buzz.backend.Utilities;
using buzz.backend.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Mvc;
using System;
using System.Linq;
using System.Net;

namespace buzz.backend.test
{
    [TestClass]
    public class QueueStackTest
    {
        [TestMethod]
        public void GetQueueStack()
        {
            QueueStackController controller = new QueueStackController();
            //QueueStack[] queuestacks = controller.Get().ToArray();
            //Assert.IsNotNull(queuestacks);
            JsonResult queuestacks = controller.Get();
            Assert.IsNotNull(queuestacks.Data);
        }

        [TestMethod]
        public void InsertUpdateDeleteQueueStack()
        {
            QueueStackController controller = new QueueStackController();

            QueueStack queuestack = new QueueStack();
            queuestack.queuestackid = Guid.NewGuid();
            queuestack.createddate = AppUtilities.DateToString();
            queuestack.modifieddate = AppUtilities.DateToString();
            queuestack.queuedate = "20180126";
            queuestack.queuenumber = "test queuestack";
            queuestack.counterid = Guid.NewGuid();
            queuestack.queuestatus = "test";
            queuestack.settime = new DateTime();
            queuestack.servetime = new DateTime();
            queuestack.endtime = new DateTime();
            queuestack.userid = "thz";
            queuestack.recordstatus = 1;

            //insert
            var result1 = controller.Save(queuestack);
            //update
            var result2 = controller.Save(queuestack);
            //delete
            var result3 = controller.Delete(queuestack.queuestackid.ToString());

            //assert
            Assert.IsNotNull(result1);
            Assert.IsNotNull(result2);
            Assert.IsNotNull(result3);
            Assert.IsTrue(((HttpStatusCodeResult)result1).StatusCode is (int)HttpStatusCode.OK);
            Assert.IsTrue(((HttpStatusCodeResult)result2).StatusCode is (int)HttpStatusCode.OK);
            Assert.IsTrue(((HttpStatusCodeResult)result3).StatusCode is (int)HttpStatusCode.OK);
        }
    }
}
