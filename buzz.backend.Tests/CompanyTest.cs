﻿using buzz.backend.Controllers;
using buzz.backend.Utilities;
using buzz.backend.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Mvc;
using System;
using System.Linq;
using System.Net;

namespace buzz.backend.test
{
    [TestClass]
    public class CompanyTest
    {
        [TestMethod]
        public void Get()
        {
            CompanyController controller = new CompanyController();
            //Company[] companys = controller.Get().ToArray();
            //Assert.IsNotNull(companys);
            JsonResult companys = controller.Get();
            Assert.IsNotNull(companys.Data);
        }

        [TestMethod]
        public void InsertUpdateDelete()
        {
            CompanyController controller = new CompanyController();

            Company company = new Company();
            company.companyid = Guid.NewGuid();
            company.createddate = AppUtilities.DateToString();
            company.modifieddate = AppUtilities.DateToString();
            company.companyname = "Test company";
            company.contact = "123";
            company.email = "test@email.com";
            company.website = "www.test.com";
            company.bdisplayonmain = false;
            company.userid = "thz";
            company.recordstatus = 1;

            //insert
            var result1 = controller.Save(company);
            //update
            var result2 = controller.Save(company);
            //delete
            var result3 = controller.Delete(company.companyid.ToString());

            //assert
            Assert.IsNotNull(result1);
            Assert.IsNotNull(result2);
            Assert.IsNotNull(result3);
            Assert.IsTrue(((HttpStatusCodeResult)result1).StatusCode is (int)HttpStatusCode.OK);
            Assert.IsTrue(((HttpStatusCodeResult)result2).StatusCode is (int)HttpStatusCode.OK);
            Assert.IsTrue(((HttpStatusCodeResult)result3).StatusCode is (int)HttpStatusCode.OK);
        }
    }
}
