﻿using buzz.backend.Controllers;
using buzz.backend.Utilities;
using buzz.backend.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Mvc;
using System;
using System.Linq;
using System.Net;

namespace buzz.backend.test
{
    [TestClass]
    public class DisplayGroupTest
    {
        [TestMethod]
        public void Get()
        {
            DisplayGroupController controller = new DisplayGroupController();
            DisplayGroup[] groups = controller.Get().ToArray();
            Assert.IsNotNull(groups);
        }

        [TestMethod]
        public void InsertUpdateDelete()
        {
            DisplayGroupController controller = new DisplayGroupController();

            DisplayGroup displaygroup = new DisplayGroup();
            displaygroup.groupid = Guid.NewGuid();
            displaygroup.createddate = AppUtilities.DateToString();
            displaygroup.modifieddate = AppUtilities.DateToString();
            displaygroup.groupname = "Test Group";
            displaygroup.userid = "thz";
            displaygroup.recordstatus = 1;

            //insert
            var result1 = controller.Save(displaygroup);
            //update
            var result2 = controller.Save(displaygroup);
            //delete
            var result3 = controller.Delete(displaygroup.groupid.ToString());

            //assert
            Assert.IsNotNull(result1);
            Assert.IsNotNull(result2);
            Assert.IsNotNull(result3);
            Assert.IsTrue(((HttpStatusCodeResult)result1).StatusCode is (int)HttpStatusCode.OK);
            Assert.IsTrue(((HttpStatusCodeResult)result2).StatusCode is (int)HttpStatusCode.OK);
            Assert.IsTrue(((HttpStatusCodeResult)result3).StatusCode is (int)HttpStatusCode.OK);
        }
    }
}
