﻿using buzz.backend.Controllers;
using buzz.backend.Utilities;
using buzz.backend.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Mvc;
using System;
using System.Linq;
using System.Net;

namespace buzz.backend.test
{
    [TestClass]
    public class CategoryTest
    {
        [TestMethod]
        public void GetCategory()
        {
            CategoryController controller = new CategoryController();
            //Category[] Categorys = controller.Get().ToArray();
            //Assert.IsNotNull(Categorys);
            JsonResult Categorys = controller.Get();
            Assert.IsNotNull(Categorys.Data);
        }

        [TestMethod]
        public void InsertUpdateDeleteCategory()
        {
            CategoryController controller = new CategoryController();

            Category Category = new Category();
            Category.categoryid = Guid.NewGuid();
            Category.createddate = AppUtilities.DateToString();
            Category.modifieddate = AppUtilities.DateToString();
            Category.categoryname = "Test Category";
            Category.userid = "thz";
            Category.recordstatus = 1;

            //insert
            var result1 = controller.Save(Category);
            //update
            var result2 = controller.Save(Category);
            //delete
            var result3 = controller.Delete(Category.categoryid.ToString());

            //assert
            Assert.IsNotNull(result1);
            Assert.IsNotNull(result2);
            Assert.IsNotNull(result3);
            Assert.IsTrue(((HttpStatusCodeResult)result1).StatusCode is (int)HttpStatusCode.OK);
            Assert.IsTrue(((HttpStatusCodeResult)result2).StatusCode is (int)HttpStatusCode.OK);
            Assert.IsTrue(((HttpStatusCodeResult)result3).StatusCode is (int)HttpStatusCode.OK);
        }
    }
}
