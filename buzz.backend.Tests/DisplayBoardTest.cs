﻿using buzz.backend.Controllers;
using buzz.backend.Utilities;
using buzz.backend.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Web.Mvc;
using System.Net;
using System.Collections.Generic;

namespace buzz.backend.test
{
    [TestClass]
    public class DisplayBoardTest
    {
        [TestMethod]
        public void Get()
        {
            DisplayBoardController controller = new DisplayBoardController();
            JsonResult doctors = controller.Get();
            Assert.IsNotNull(doctors.Data);
        }

        [TestMethod]
        public void InsertUpdateDelete()
        {
            DisplayBoardController controller = new DisplayBoardController();

            DisplayBoard board = new DisplayBoard();
            board.displayboardid = Guid.NewGuid();
            board.createddate = AppUtilities.DateToString();
            board.modifieddate = AppUtilities.DateToString();
            board.displayboardname = "Test name";
            board.groupid = Guid.NewGuid();
            board.mainboardid = Guid.NewGuid();
            board.templateid = Guid.NewGuid();
            board.displayorder = 1;
            board.status = 1;
            board.userid = "thz";
            board.recordstatus = 1;

            //insert
            var result1 = controller.Save(board);
            //update
            var result2 = controller.Save(board);
            //delete
            var result3 = controller.Delete(board.displayboardid.ToString());

            //assert
            Assert.IsNotNull(result1);
            Assert.IsNotNull(result2);
            Assert.IsNotNull(result3);
            Assert.IsTrue(((HttpStatusCodeResult)result1).StatusCode is (int)HttpStatusCode.OK);
            Assert.IsTrue(((HttpStatusCodeResult)result2).StatusCode is (int)HttpStatusCode.OK);
            Assert.IsTrue(((HttpStatusCodeResult)result3).StatusCode is (int)HttpStatusCode.OK);
        }
    }
}
