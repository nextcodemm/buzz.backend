﻿using buzz.backend.Controllers;
using buzz.backend.Utilities;
using buzz.backend.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Mvc;
using System;
using System.Linq;
using System.Net;
using System.Collections.Generic;
using System.Web.Script.Serialization;

namespace buzz.backend.test
{
    [TestClass]
    public class DisplayBoardSettingTest
    {
        [TestMethod]
        public void Get()
        {
            DisplayBoardSettingController controller = new DisplayBoardSettingController();
            JsonResult resultData = controller.Get();
            Assert.IsNotNull(resultData.Data);
        }

        [TestMethod]
        public void InsertUpdateDelete()
        {
            DisplayBoardSettingController controller = new DisplayBoardSettingController();

            DisplayBoardSetting DisplayBoardSetting = new DisplayBoardSetting();
            DisplayBoardSetting.settingid = Guid.NewGuid();
            DisplayBoardSetting.createddate = AppUtilities.DateToString();
            DisplayBoardSetting.modifieddate = AppUtilities.DateToString();
            DisplayBoardSetting.displayboardid = Guid.NewGuid();
            DisplayBoardSetting.doctorid = Guid.NewGuid();
            DisplayBoardSetting.notshowtime = false;
            DisplayBoardSetting.starttime = "20171213";
            DisplayBoardSetting.endtime = "20171213";
            DisplayBoardSetting.userid = "thz";
            DisplayBoardSetting.recordstatus = 1;

            //insert
            var result1 = controller.Save(DisplayBoardSetting);
            //update
            var result2 = controller.Save(DisplayBoardSetting);
            //delete
            var result3 = controller.Delete(DisplayBoardSetting.settingid.ToString());

            //assert
            Assert.IsNotNull(result1);
            Assert.IsNotNull(result2);
            Assert.IsNotNull(result3);
            Assert.IsTrue(((HttpStatusCodeResult)result1).StatusCode is (int)HttpStatusCode.OK);
            Assert.IsTrue(((HttpStatusCodeResult)result2).StatusCode is (int)HttpStatusCode.OK);
            Assert.IsTrue(((HttpStatusCodeResult)result3).StatusCode is (int)HttpStatusCode.OK);
        }
    }
}
