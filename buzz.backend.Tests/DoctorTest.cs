﻿using buzz.backend.Controllers;
using buzz.backend.Utilities;
using buzz.backend.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Mvc;
using System;
using System.Linq;
using System.Net;

namespace buzz.backend.test
{
    [TestClass]
    public class DoctorTest
    {
        [TestMethod]
        public void Get()
        {
            DoctorController controller = new DoctorController();
            JsonResult doctors = controller.Get();
            Assert.IsNotNull(doctors.Data);
        }

        [TestMethod]
        public void InsertUpdateDelete()
        {
            DoctorController controller = new DoctorController();

            Doctor doctor = new Doctor();
            doctor.doctorid = Guid.NewGuid();
            doctor.createddate = AppUtilities.DateToString();
            doctor.modifieddate = AppUtilities.DateToString();
            doctor.doctorname = "Test doctor";
            doctor.speciality = "test speciality";
            doctor.degree = "test degree";
            doctor.rank = "test rank";
            doctor.department = "test department";
            doctor.licencenumber = "test licencenumber";
            doctor.gender = "Female";
            doctor.address = "test address";
            doctor.homephone = "123456789";
            doctor.mobile = "123456789";
            doctor.email = "test@test.com";
            doctor.userid = "thz";
            doctor.recordstatus = 1;

            //insert
            var result1 = controller.Save(doctor, null);
            //update
            var result2 = controller.Save(doctor, null);
            //delete
            var result3 = controller.Delete(doctor.doctorid.ToString());

            //assert
            Assert.IsNotNull(result1);
            Assert.IsNotNull(result2);
            Assert.IsNotNull(result3);
            Assert.IsTrue(((HttpStatusCodeResult)result1).StatusCode is (int)HttpStatusCode.OK);
            Assert.IsTrue(((HttpStatusCodeResult)result2).StatusCode is (int)HttpStatusCode.OK);
            Assert.IsTrue(((HttpStatusCodeResult)result3).StatusCode is (int)HttpStatusCode.OK);
        }
    }
}
