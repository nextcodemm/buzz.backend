﻿using buzz.backend.Controllers;
using buzz.backend.Utilities;
using buzz.backend.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Mvc;
using System;
using System.Linq;
using System.Net;

namespace buzz.backend.test
{
    [TestClass]
    public class QueueCounterTest
    {
        [TestMethod]
        public void GetQueueCounter()
        {
            QueueCounterController controller = new QueueCounterController();
            //QueueCounter[] queuecounters = controller.Get().ToArray();
            //Assert.IsNotNull(queuecounters);
            JsonResult queuecounters = controller.Get();
            Assert.IsNotNull(queuecounters.Data);
        }

        [TestMethod]
        public void InsertUpdateDeleteQueueCounter()
        {
            QueueCounterController controller = new QueueCounterController();

            QueueCounter queuecounter = new QueueCounter();
            queuecounter.queuecounterid = Guid.NewGuid();
            queuecounter.createddate = AppUtilities.DateToString();
            queuecounter.modifieddate = AppUtilities.DateToString();
            queuecounter.countername = "Test queuecounter";
            queuecounter.backcolor = "test queuecounter";
            queuecounter.userid = "thz";
            queuecounter.recordstatus = 1;

            //insert
            var result1 = controller.Save(queuecounter);
            //update
            var result2 = controller.Save(queuecounter);
            //delete
            var result3 = controller.Delete(queuecounter.queuecounterid.ToString());

            //assert
            Assert.IsNotNull(result1);
            Assert.IsNotNull(result2);
            Assert.IsNotNull(result3);
            Assert.IsTrue(((HttpStatusCodeResult)result1).StatusCode is (int)HttpStatusCode.OK);
            Assert.IsTrue(((HttpStatusCodeResult)result2).StatusCode is (int)HttpStatusCode.OK);
            Assert.IsTrue(((HttpStatusCodeResult)result3).StatusCode is (int)HttpStatusCode.OK);
        }
    }
}
