﻿using buzz.backend.Controllers;
using buzz.backend.Utilities;
using buzz.backend.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Mvc;
using System;
using System.Linq;
using System.Net;

namespace buzz.backend.test
{
    [TestClass]
    public class TemplateTest
    {
        [TestMethod]
        public void Get()
        {
            TemplateController controller = new TemplateController();
            //Template[] templates = controller.Get().ToArray();
            //Assert.IsNotNull(templates);
            JsonResult templates = controller.Get();
            Assert.IsNotNull(templates.Data);
        }

        [TestMethod]
        public void InsertUpdateDelete()
        {
            TemplateController controller = new TemplateController();

            Template template = new Template();
            template.templateid = Guid.NewGuid();
            template.createddate = AppUtilities.DateToString();
            template.modifieddate = AppUtilities.DateToString();
            template.templatename = "Test template";
            template.templateurl = "test template";
            template.userid = "thz";
            template.recordstatus = 1;

            //insert
            var result1 = controller.Save(template);
            //update
            var result2 = controller.Save(template);
            //delete
            var result3 = controller.Delete(template.templateid.ToString());

            //assert
            Assert.IsNotNull(result1);
            Assert.IsNotNull(result2);
            Assert.IsNotNull(result3);
            Assert.IsTrue(((HttpStatusCodeResult)result1).StatusCode is (int)HttpStatusCode.OK);
            Assert.IsTrue(((HttpStatusCodeResult)result2).StatusCode is (int)HttpStatusCode.OK);
            Assert.IsTrue(((HttpStatusCodeResult)result3).StatusCode is (int)HttpStatusCode.OK);
        }
    }
}
