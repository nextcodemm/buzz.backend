USE [master]
GO
/****** Object:  Database [Buzz]    Script Date: 7/1/2018 12:35:41 PM ******/
CREATE DATABASE [Buzz]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Buzz', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\Buzz.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Buzz_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\Buzz_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [Buzz] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Buzz].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Buzz] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Buzz] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Buzz] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Buzz] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Buzz] SET ARITHABORT OFF 
GO
ALTER DATABASE [Buzz] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Buzz] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Buzz] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Buzz] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Buzz] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Buzz] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Buzz] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Buzz] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Buzz] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Buzz] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Buzz] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Buzz] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Buzz] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Buzz] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Buzz] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Buzz] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Buzz] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Buzz] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Buzz] SET  MULTI_USER 
GO
ALTER DATABASE [Buzz] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Buzz] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Buzz] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Buzz] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Buzz] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [Buzz] SET QUERY_STORE = OFF
GO
USE [Buzz]
GO
ALTER DATABASE SCOPED CONFIGURATION SET IDENTITY_CACHE = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [Buzz]
GO
/****** Object:  Table [dbo].[Category]    Script Date: 7/1/2018 12:35:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[categoryid] [uniqueidentifier] NOT NULL,
	[createddate] [char](8) NULL,
	[modifieddate] [char](8) NULL,
	[categoryname] [nvarchar](200) NULL,
	[userid] [varchar](50) NULL,
	[recordstatus] [tinyint] NULL,
 CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED 
(
	[categoryid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Comments]    Script Date: 7/1/2018 12:35:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Comments](
	[commentid] [uniqueidentifier] NOT NULL,
	[postid] [uniqueidentifier] NULL,
	[score] [int] NULL,
	[text] [nvarchar](max) NULL,
	[creationdate] [datetime] NULL,
	[userdisplayname] [nvarchar](255) NULL,
	[userid] [uniqueidentifier] NULL,
 CONSTRAINT [PK_Comments] PRIMARY KEY CLUSTERED 
(
	[commentid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Doctor]    Script Date: 7/1/2018 12:35:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Doctor](
	[doctorid] [uniqueidentifier] NOT NULL,
	[createddate] [char](8) NULL,
	[modifieddate] [char](8) NULL,
	[doctorname] [nvarchar](255) NULL,
	[categoryid] [uniqueidentifier] NULL,
	[speciality] [nvarchar](255) NULL,
	[degree] [varchar](255) NULL,
	[rank] [nvarchar](255) NULL,
	[department] [nvarchar](100) NULL,
	[licencenumber] [nvarchar](50) NULL,
	[gender] [varchar](50) NULL,
	[city] [nvarchar](100) NULL,
	[address] [nvarchar](255) NULL,
	[homephone] [varchar](50) NULL,
	[mobile] [varchar](50) NULL,
	[email] [varchar](50) NULL,
	[userid] [varchar](50) NULL,
	[recordstatus] [tinyint] NULL,
	[doctorsource] [nvarchar](max) NULL,
	[useraccountid] [uniqueidentifier] NULL,
 CONSTRAINT [PK_Doctor] PRIMARY KEY CLUSTERED 
(
	[doctorid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Posts]    Script Date: 7/1/2018 12:35:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Posts](
	[postid] [uniqueidentifier] NOT NULL,
	[posttypeid] [int] NULL,
	[acceptedanswerid] [uniqueidentifier] NULL,
	[parentid] [uniqueidentifier] NULL,
	[creationdate] [datetime] NULL,
	[score] [int] NULL,
	[viewcount] [int] NULL,
	[body] [nvarchar](max) NULL,
	[owneruserid] [uniqueidentifier] NULL,
	[ownerdisplayname] [nvarchar](255) NULL,
	[lasteditoruserid] [uniqueidentifier] NULL,
	[lasteditordisplayname] [nvarchar](255) NULL,
	[lasteditdate] [nvarchar](50) NULL,
	[lastactivitydate] [nvarchar](50) NULL,
	[title] [nvarchar](255) NULL,
	[tags] [nvarchar](255) NULL,
	[answercount] [int] NULL,
	[commentcount] [int] NULL,
	[favoritecount] [int] NULL,
	[closeddate] [nvarchar](50) NULL,
	[communityowneddate] [nvarchar](50) NULL,
 CONSTRAINT [PK_Posts] PRIMARY KEY CLUSTERED 
(
	[postid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 7/1/2018 12:35:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[userid] [uniqueidentifier] NOT NULL,
	[reputation] [decimal](18, 0) NULL,
	[creationdate] [nvarchar](50) NULL,
	[displayname] [nvarchar](255) NULL,
	[lastaccessdate] [nvarchar](50) NULL,
	[websiteurl] [nvarchar](255) NULL,
	[location] [nvarchar](max) NULL,
	[aboutme] [nvarchar](max) NULL,
	[views] [int] NULL,
	[upvotes] [int] NULL,
	[downvotes] [int] NULL,
	[email] [nvarchar](255) NULL,
	[password] [nvarchar](255) NULL,
	[age] [int] NULL,
	[accountid] [uniqueidentifier] NULL,
	[isdoctor] [int] NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[userid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[usplogin]    Script Date: 7/1/2018 12:35:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usplogin] 
	@email      NVARCHAR(254),
	@password       NVARCHAR(50),
	@responseMessage INT=0 output
AS

BEGIN

    SET nocount ON

    DECLARE @userID UNIQUEIDENTIFIER

    IF EXISTS (SELECT	userid
               FROM		[dbo].[Users]
               WHERE	email = @email)
		BEGIN
			SET @userID=	(
							SELECT	userid
							FROM	[dbo].[Users]
							WHERE	email = @email
							AND		password = @password 
							)

			IF	( @userID IS NULL )
				SET @responseMessage=1--'Incorrect password'
			ELSE
				SET @responseMessage=2--'Successful login'
		END

    ELSE
		SET @responseMessage=3--'Invalid login'

	RETURN @responseMessage
END 
GO
USE [master]
GO
ALTER DATABASE [Buzz] SET  READ_WRITE 
GO
