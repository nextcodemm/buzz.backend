﻿using Microsoft.Owin;
using Owin;
using System.Text;
using System;
using Microsoft.Owin.Security.OAuth;
using Microsoft.Owin.Security.Jwt;
using Microsoft.Owin.Security.DataHandler.Encoder;
using Microsoft.Owin.Security;
using System.IdentityModel.Tokens;
using Microsoft.AspNet.SignalR;
using Microsoft.Owin.Cors;

[assembly: OwinStartup(typeof(buzz.backend.Startup))]
namespace buzz.backend
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {

            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
            app.MapSignalR();
            //app.Map("/signalr", map =>
            //{
            //    // Setup the CORS middleware to run before SignalR.
            //    // By default this will allow all origins. You can 
            //    // configure the set of origins and/or http verbs by
            //    // providing a cors options with a different policy.
            //    map.UseCors(CorsOptions.AllowAll);
            //    //map.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
            //    var hubConfiguration = new HubConfiguration
            //    {
            //        // You can enable JSONP by uncommenting line below.
            //        // JSONP requests are insecure but some older browsers (and some
            //        // versions of IE) require JSONP to work cross domain
            //        EnableJSONP = true
            //    };
            //    // Run the SignalR pipeline. We're not using MapSignalR
            //    // since this branch already runs under the "/signalr"
            //    // path.
            //    map.RunSignalR(hubConfiguration);
            //});

            ConfigureOAuthTokenConsumption(app);
        }

        private void ConfigureOAuthTokenConsumption(IAppBuilder app)
        {
            var key = new InMemorySymmetricSecurityKey(Encoding.UTF8.GetBytes("nextcode@20171001"));

            TokenValidationParameters tvps = new TokenValidationParameters
            {
                //ValidateIssuerSigningKey = true, //only support in JWT V-5.xss
                IssuerSigningKey = new InMemorySymmetricSecurityKey(Encoding.UTF8.GetBytes("nextcode@20171001")),

                ValidateIssuer = true,
                ValidIssuer = "HospiBook",

                ValidateAudience = true,
                ValidAudience = "HospiBookWeb",

                ValidateLifetime = true, //validate the expiration and not before values in the token

                ClockSkew = TimeSpan.FromMinutes(5) //5 minute tolerance for the expiration date
            };

            app.UseJwtBearerAuthentication(new JwtBearerAuthenticationOptions
            {
                TokenValidationParameters = tvps
            });
        }
    }
}
