﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using System.Threading.Tasks;
using buzz.backend.Models;

namespace buzz.backend.Hubs
{
    public class QueueHub : Hub
    {
        public static List<KeyValuePair<string, string>> GroupList = new List<KeyValuePair<string, string>>();

        public void SendQueueSetting(string clientid, string queuecounterid)
        {
            try
            {
                var groupList = GroupList.Where(g => g.Key.ToUpper() == clientid.ToUpper()).ToList();
                if (groupList.Count > 0)
                {
                    foreach (var group in GroupList)
                    {
                        Clients.Client(group.Value).updateQueueDisplay(queuecounterid);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SendUpcommingSetting(string queuecountercategoryid)
        {
            try
            {
                Clients.All.updateUpcomingDisplay(queuecountercategoryid);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public override Task OnConnected()
        {
            var clientid = Context.QueryString["clientid"];
            if (!string.IsNullOrEmpty(clientid))
            {
                var group = GroupList.Where(g => g.Key.ToUpper() == clientid.ToUpper() && g.Value.ToUpper() == Context.ConnectionId.ToUpper()).SingleOrDefault();

                if (!GroupList.Contains(group))
                {
                    GroupList.Add(new KeyValuePair<string, string>(clientid, Context.ConnectionId));
                }
            }

            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            var group = GroupList.Where(g => g.Value.ToUpper() == Context.ConnectionId.ToUpper()).SingleOrDefault();
            GroupList.Remove(group);

            return base.OnDisconnected(stopCalled);
        }

        public override Task OnReconnected()
        {
            var group = GroupList.Where(g => g.Value.ToUpper() == Context.ConnectionId.ToUpper()).SingleOrDefault();

            if (!GroupList.Contains(group))
            {
                var clientid = Context.QueryString["clientid"];
                if (!string.IsNullOrEmpty(clientid))
                {
                    GroupList.Add(new KeyValuePair<string, string>(clientid, Context.ConnectionId));
                }
            }

            return base.OnReconnected();
        }
    }
}