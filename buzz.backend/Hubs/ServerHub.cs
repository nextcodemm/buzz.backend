﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using System.Threading.Tasks;
using buzz.backend.Models;

namespace buzz.backend.Hubs
{
    public class ServerHub : Hub
    {
        public static List<KeyValuePair<string, string>> GroupList = new List<KeyValuePair<string, string>>();

        public void SendClientInfo()
        {
            Clients.All.updateClientInfo(GroupList);
        }

        public void SendMessage(string name, string message)
        {
            Clients.All.broadcastMessage(name, message);
        }

        public void SendMessage(string name, string message, string clientid)
        {
            var group = GroupList.Where(g => g.Key.ToUpper() == clientid.ToUpper()).SingleOrDefault();

            Clients.Client(group.Value).broadcastMessage(name, message);
        }

        //public void SendClientId(string clientid)
        //{
        //    if(!string.IsNullOrEmpty(clientid))
        //    {
        //        var group = GroupList.Where(g => g.Key.ToUpper() == clientid.ToUpper() && g.Value.ToUpper() == Context.ConnectionId.ToUpper()).SingleOrDefault();

        //        if (!GroupList.Contains(group))
        //        {
        //            GroupList.Add(new KeyValuePair<string, string>(clientid, Context.ConnectionId));
        //        }
        //    }
        //}

        public void SendDisplayBoardSetting(DisplayBoardSetting displayBoardSetting, string clientid)
        {
            try
            {
                var groupList = GroupList.Where(g => g.Key.ToUpper() == clientid.ToUpper()).ToList();
                if (groupList.Count > 0)
                {
                    foreach (var group in groupList)
                    {
                        Clients.Client(group.Value).updateDisplayBoardDisplay(displayBoardSetting);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SendMainBoardSetting(string mainboardid, string clientid)
        {
            try
            {
                var groupList = GroupList.Where(g => g.Key.ToUpper() == clientid.ToUpper()).ToList();
                if (groupList.Count > 0)
                {
                    foreach (var group in groupList)
                    {
                        Clients.Client(group.Value).updateMainBoardDisplay(mainboardid);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public override Task OnConnected()
        {
            var clientid = Context.QueryString["clientid"];
            if (!string.IsNullOrEmpty(clientid))
            {
                var group = GroupList.Where(g => g.Key.ToUpper() == clientid.ToUpper() && g.Value.ToUpper() == Context.ConnectionId.ToUpper()).SingleOrDefault();

                if (!GroupList.Contains(group))
                {
                    GroupList.Add(new KeyValuePair<string, string>(clientid, Context.ConnectionId));
                }
            }

            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            var group = GroupList.Where(g => g.Value.ToUpper() == Context.ConnectionId.ToUpper()).SingleOrDefault();
            GroupList.Remove(group);

            return base.OnDisconnected(stopCalled);
        }

        public override Task OnReconnected()
        {
            var group = GroupList.Where(g => g.Value.ToUpper() == Context.ConnectionId.ToUpper()).SingleOrDefault();

            if (!GroupList.Contains(group))
            {
                var clientid = Context.QueryString["clientid"];
                if(!string.IsNullOrEmpty(clientid))
                {
                    GroupList.Add(new KeyValuePair<string, string>(clientid, Context.ConnectionId));
                }
            }

            return base.OnReconnected();
        }
    }
}