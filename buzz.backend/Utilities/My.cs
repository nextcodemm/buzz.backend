﻿using System;
using System.Data.Common;
using System.Data.SqlClient;

namespace buzz.backend.Utilities
{
    public static class My
    {
        public static Func<DbConnection> ConnectionFactory = () => new SqlConnection(Shared.con_string);

        #region "table definisions"

        public static DbTable Table_UserAccount = new DbTable
        {
            Name = "UserAccount",
            Columns = new string[] { "useraccountid", "createddate", "modifieddate", "username", "email", "mobile", "accountstatus", "profilepicture", "verifycode", "verifystatus", "userid", "recordstatus" },
            Keys = new string[] { "useraccountid" }
        };

        public static DbTable Table_HealthCareUnitType = new DbTable
        {
            Name = "HealthCareUnitType",
            Columns = new string[] { "unittypeid", "createddate", "modifieddate", "unittype", "userid", "recordstatus" },
            Keys = new string[] { "unittypeid" }
        };

        public static DbTable Table_HealthCareUnit = new DbTable
        {
            Name = "HealthCareUnit",
            Columns = new string[] { "healthcareunitid", "createddate", "modifieddate", "healthcareunitname", "unittypeid", "address", "phone", "email", "contactperson",
                                     "servername", "databasename", "databaseuser", "databasepassword", "userid", "recordstatus" },
            Keys = new string[] { "healthcareunitid" }
        };

        public static DbTable Table_HealthCareUnitUser = new DbTable
        {
            Name = "HealthCareUnitUser",
            Columns = new string[] { "healthcareunituserid", "healthcareunitid", "useraccountid", "createddate", "modifieddate", "userid", "recordstatus" },
            Keys = new string[] { "healthcareunituserid" }
        };

        public static DbTable Table_Doctor = new DbTable
        {
            Name = "Doctor",
            Columns = new string[] { "doctorid ","doctorsource", "createddate", "modifieddate", "doctorname", "categoryid", "speciality", "degree", "rank", "department", "licencenumber",
                                     "gender", "address", "homephone", "mobile", "email", "userid", "recordstatus","useraccountid" },
            Keys = new string[] { "doctorid" }
        };

        public static DbTable Table_DisplayGroup = new DbTable
        {
            Name = "DisplayGroup",
            Columns = new string[] { "groupid ", "createddate", "modifieddate", "groupname", "userid", "recordstatus" },
            Keys = new string[] { "groupid" }
        };

        public static DbTable Table_DisplayBoard = new DbTable
        {
            Name = "DisplayBoard",
            Columns = new string[] { "displayboardid ", "createddate", "modifieddate", "displayboardname", "groupid", "mainboardid", "templateid", "displayorder", "status", "userid", "recordstatus" },
            Keys = new string[] { "displayboardid" }
        };

        public static DbTable Table_DisplayBoardSetting = new DbTable
        {
            Name = "DisplayBoardSetting",
            Columns = new string[] { "settingid", "createddate", "modifieddate", "displayboardid", "doctorid", "notshowad",  "notshowtime", "systemstarttime", "systemendtime",  "starttime", "endtime", "userid", "recordstatus" },
            Keys = new string[] { "settingid" }
        };

        public static DbTable Table_DisplayBoardSetting_Log = new DbTable
        {
            Name = "DisplayBoardSetting_Log",
            Columns = new string[] { "settinglogid", "settingid", "createddate", "modifieddate", "displayboardid", "doctorid", "notshowad", "notshowtime", "systemstarttime", "systemendtime", "starttime", "endtime", "userid", "recordstatus" },
            Keys = new string[] { "settingid" }
        };

        public static DbTable Table_Ads = new DbTable
        {
            Name = "Ads",
            Columns = new string[] { "adsid ", "adsgroupid", "createddate", "modifieddate", "adsname", "adscompany", "adssource", "durationinseconds", "userid", "recordstatus" },
            Keys = new string[] { "adsid" }
        };

        public static DbTable Table_AdsSetting = new DbTable
        {
            Name = "AdsSetting",
            Columns = new string[] { "adssettingid", "createddate", "modifieddate", "adsgroupid", "displayboardid", "userid", "recordstatus" },
            Keys = new string[] { "adssettingid" }
        };

        public static DbTable Table_Category = new DbTable
        {
            Name = "Category",
            Columns = new string[] { "categoryid", "createddate", "modifieddate", "categoryname", "userid", "recordstatus" },
            Keys = new string[] { "categoryid" }
        };

        public static DbTable Table_QueueCounter = new DbTable
        {
            Name = "QueueCounter",
            Columns = new string[] { "queuecounterid ", "createddate", "modifieddate", "countername", "countercategoryid", "displayorder", "backcolor", "userid", "recordstatus" },
            Keys = new string[] { "queuecounterid" }
        };

        public static DbTable Table_QueueStack = new DbTable
        {
            Name = "QueueStack",
            Columns = new string[] { "queuestackid", "createddate", "modifieddate", "queuedate", "queuenumber", "countercategoryid", "counterid", "queuestatus", "queueagainorder", "settime", "reservetime", "servetime", "endtime", "userid", "recordstatus" },
            Keys = new string[] { "queuestackid" }
        };

        public static DbTable Table_Company = new DbTable
        {
            Name = "Company",
            Columns = new string[] { "companyid", "createddate", "modifieddate", "companyname", "contact", "email", "website", "otherinformation", "bdisplayonmain", "userid", "recordstatus" },
            Keys = new string[] { "companyid" }
        };

        public static DbTable Table_Template = new DbTable
        {
            Name = "Template",
            Columns = new string[] { "templateid ", "createddate", "modifieddate", "templatename", "templateurl", "userid", "recordstatus" },
            Keys = new string[] { "templateid" }
        };

        public static DbTable Table_DoctorSchedule = new DbTable
        {
            Name = "DoctorSchedule",
            Columns = new string[] {"doctorscheduleid","createddate","modifieddate","doctorid","healthcareunitid","day","starttime","endtime","userid","recordstatus" },
            Keys = new string[] { "doctorscheduleid" }
        };

        public static DbTable Table_Appointment = new DbTable
        {
            Name = "Appointment",
            Columns = new string[] { "appointmentid", "createddate", "modifieddate", "doctorid", "patientid", "healthcareunitid", "date", "starttime", "endtime", "userid", "recordstatus" },
            Keys = new string[] { "appointmentid" }
        };

        public static DbTable Table_Users = new DbTable
        {
            Name = "Users",
            Columns = new string[] { "userid", "reputation", "creationdate", "displayname", "lastaccessdate", "websiteurl", "location",
                "aboutme", "views", "upvotes", "downvotes", "email", "password", "accountid", "age","isdoctor" },
            Keys = new string[] { "userid" }
        };

        public static DbTable Table_Posts = new DbTable
        {
            Name = "Posts",
            Columns = new string[] {"postid", "posttypeid", "acceptedanswerid", "parentid", "creationdate", "score",
                                "viewcount", "body", "owneruserid", "ownerdisplayname", "lasteditoruserid", "lasteditordisplayname",
                                "lasteditdate", "lastactivitydate", "title", "tags", "answercount" , "commentcount", "favoritecount",
                                    "closeddate", "communityowneddate"},
            Keys = new string[] { "postid" }
        };

        public static DbTable Table_Comments = new DbTable
        {
            Name = "Comments",
            Columns = new string[] { "commentid" , "postid", "score", "text", "creationdate", "userdisplayname","userid"},
            Keys = new string[] { "commentid" }
        };


        #endregion

        #region "extension functions"
        public static string GetColumnNames(this string[] columns)
        {
            string result = "";
            foreach (string c in columns)
            {
                result = result + $"{c}, ";
            }
            return result.Trim().TrimEnd(new char[] { ',' });
        }

        public static string GetInsertParams(this string[] columns)
        {
            string result = "";
            foreach (string c in columns)
            {
                result = result + $"@{c}, ";
            }
            return result.Trim().TrimEnd(new char[] { ',' });
        }

        public static string GetUpdateParams(this string[] columns)
        {
            string result = "";
            foreach (string c in columns)
            {
                result = result + $"{c}=@{c}, ";
            }
            return result.Trim().TrimEnd(new char[] { ',' });
        }

        public static string GetWhereParams(this string[] columns)
        {
            string result = "";
            foreach (string c in columns)
            {
                result += $"{c}=@{c} AND ";
            }
            return result.Trim().TrimEnd(new char[] { 'A', 'N', 'D' });
        }

        public static string GetCompositParams(this string[] columns)
        {
            string result = "";
            foreach (string c in columns)
            {
                result += $"{c}=@{c} AND ";
            }
            return result.Trim().TrimEnd(new char[] { 'A', 'N', 'D' });
        }
        #endregion
    }
}
