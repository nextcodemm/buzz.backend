﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace buzz.backend.Utilities
{
    public class DbTable
    {
        public string Name { get; set; }
        public string[] Columns { get; set; }
        public string[] Keys { get; set; }
        public string[] CompositKeys { get; set; }
        public string MaxId { get; set; }

        public string SelectMaxId => $"SELECT MAX({MaxId}) FROM {Name}";
        public string SelectMaxIdWithComposit => $"SELECT MAX({MaxId}) FROM {Name} WHERE {CompositKeys.GetCompositParams()}";
        public string Select => $"SELECT * FROM {Name} WHERE recordstatus<>2";
        public string SelectSingle => $"SELECT * FROM {Name} {Where}";
        public string Insert => $"INSERT INTO {Name} ({Columns.GetColumnNames()}) VALUES({Columns.GetInsertParams()})";
        public string Update => $"UPDATE {Name} SET {Columns.GetUpdateParams()} {Where}";
        public string Delete => $"DELETE FROM {Name} {Where}";
        public string Where => $" WHERE {Keys.GetWhereParams()}";
    }
}
