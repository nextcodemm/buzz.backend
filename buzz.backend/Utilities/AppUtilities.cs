﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace buzz.backend.Utilities
{
    public class AppUtilities
    {
        #region --Numeric Method--
        public static bool IsNumeric(object aExpression)
        {
            if (aExpression == null) { aExpression = ""; }
            return IsNumeric(Convert.ToString(aExpression));
        }

        public static bool IsNumeric(string aExpression)
        {
            bool l_IsNum;
            double l_ReturnNum;
            l_IsNum = Double.TryParse(Convert.ToString(aExpression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out l_ReturnNum);
            return l_IsNum;
        }
        #endregion

        #region --Change Date To String Eg.20081220--
        private static DateTime GetSystemDateTime()
        {
            return DateTime.Now;
        }

        public static string DateToString()
        {
            return DateToString(GetSystemDateTime());
        }

        public static string DateToString(DateTime aDT)
        {
            try
            {
                try
                {
                    DateTime l_datetime = aDT;
                    return l_datetime.Year.ToString("0000") + l_datetime.Month.ToString("00") + l_datetime.Day.ToString("00");
                }
                catch
                {
                    return "";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string DateToString(string aDate)
        {
            if (aDate.Equals(string.Empty)) return "";
            if (aDate == null || aDate.Length == 0)
            {
                throw new Exception("Parameter cannot be null or empty!");
            }
            try
            {
                try
                {
                    if (aDate.Substring(0, 2).Equals("00"))
                    {
                        return aDate.Substring(6, 4) + "0000";
                    }
                    DateTime l_datetime = DateTime.Parse(aDate);
                    return l_datetime.Year.ToString("0000") + l_datetime.Month.ToString("00") + l_datetime.Day.ToString("00");
                }
                catch (FormatException ex)
                {
                    throw ex;
                }
                catch (ArgumentNullException ex)
                {
                    throw ex;
                }
                catch (ArgumentException ex)
                {
                    throw ex;
                }
                catch
                {
                    return aDate.Substring(6, 4) + aDate.Substring(3, 2) + aDate.Substring(0, 2);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetDay(string monthdate)
        {
            DateTime l_date = Convert.ToDateTime(StringToDate(monthdate));
            return l_date.DayOfWeek.ToString();
        }
        #endregion

        #region --Change String To Date Eg.20/12/2008--
        public static string StringToDate()
        {
            return GetSystemDateTime().ToShortDateString();
        }

        public static string StringToDate(string aString)
        {
            if (aString == null || aString.Length == 0)
            {
                throw new Exception("Parameter cannot be null or empty");
            }
            if (aString.Length < 8)
            {
                throw new Exception("Too short to convert!");
            }
            try
            {
                if (aString.Substring(4, 4).Equals("0000"))
                {
                    return aString.Substring(6, 2) + "/" + aString.Substring(4, 2) + "/" + aString.Substring(0, 4);
                }
                else
                {
                    return new DateTime(int.Parse(aString.Substring(0, 4)), int.Parse(aString.Substring(4, 2)), int.Parse(aString.Substring(6, 2))).ToShortDateString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string StringToDate(string aString, string aFormat)
        {
            if (aString == null || aString.Length == 0)
            {
                throw new Exception("Parameter cannot be null or empty");
            }
            if (aString.Length < 8)
            {
                throw new Exception("Too short to convert!");
            }
            try
            {
                if (aString.Substring(4, 4).Equals("0000"))
                {
                    return aString.Substring(6, 2) + "/" + aString.Substring(4, 2) + "/" + aString.Substring(0, 4);
                }
                else
                {
                    return new DateTime(int.Parse(aString.Substring(0, 4)), int.Parse(aString.Substring(4, 2)), int.Parse(aString.Substring(6, 2))).ToString(aFormat);
                }
            }
            catch
            {
                throw;
            }
        }

        public static string GetMonthFromString(string month)
        {
            int monthInt = Convert.ToInt32(month);
            string monthStr = "";

            switch (monthInt)
            {
                case 1:
                    monthStr = "January";
                    break;
                case 2:
                    monthStr = "February";
                    break;
                case 3:
                    monthStr = "March";
                    break;
                case 4:
                    monthStr = "April";
                    break;
                case 5:
                    monthStr = "May";
                    break;
                case 6:
                    monthStr = "Jun";
                    break;
                case 7:
                    monthStr = "July";
                    break;
                case 8:
                    monthStr = "August";
                    break;
                case 9:
                    monthStr = "September";
                    break;
                case 10:
                    monthStr = "October";
                    break;
                case 11:
                    monthStr = "November";
                    break;
                case 12:
                    monthStr = "December";
                    break;
                default:
                    break;
            }

            return monthStr;
        }
        #endregion

        #region --Change String To Time Eg.10:34:00--
        public static string StringToTime(string aDate, string aTime)
        {
            if (aTime == null || aTime.Length == 0)
            {
                throw new Exception("Parameter cannot be null or empty!");
            }
            if (aTime.Length < 6)
            {
                throw new Exception("Too short to convert!");
            }
            try
            {
                if (aDate == null || aDate.Equals(string.Empty) | aDate.Length < 8)
                {
                    return new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, int.Parse(aTime.Substring(0, 2)), int.Parse(aTime.Substring(2, 2)), int.Parse(aTime.Substring(4, 2))).ToShortTimeString();
                }
                else
                {
                    return new DateTime(int.Parse(aDate.Substring(0, 4)), int.Parse(aDate.Substring(4, 2)), int.Parse(aDate.Substring(6, 2)), int.Parse(aTime.Substring(0, 2)), int.Parse(aTime.Substring(2, 2)), int.Parse(aTime.Substring(4, 2))).ToShortTimeString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string StringToTime(string aTime)
        {
            if (aTime.Equals(string.Empty)) return "";
            if (aTime == null || aTime.Length == 0)
            {
                throw new Exception("Parameter cannot be null or empty");
            }
            if (aTime.Length < 6)
            {
                throw new Exception("Too short to convert!");
            }
            try
            {
                return new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, int.Parse(aTime.Substring(0, 2)), int.Parse(aTime.Substring(2, 2)), int.Parse(aTime.Substring(4, 2))).ToShortTimeString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string StringToTime()
        {
            return GetSystemDateTime().ToShortTimeString();
        }
        #endregion

        #region --Change Time To String Eg.103400--
        public static string TimeToString()
        {
            return TimeToString(GetSystemDateTime());
        }

        public static string TimeToString(DateTime aTime)
        {
            try
            {
                try
                {
                    DateTime l_Time = aTime;
                    return l_Time.Hour.ToString("00") + l_Time.Minute.ToString("00") + l_Time.Second.ToString("00");
                }
                catch
                {
                    return "";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region --Number Formatting Methods--
        public static string FormatNumber(double aAmount)
        {
            return aAmount.ToString("###,##0.00");
        }

        public static string FormatNumber(double aAmount, string aFormat)
        {
            return aAmount.ToString(aFormat);
        }

        public static string FormatNumber(decimal aAmount)
        {
            return aAmount.ToString("###,##0.00");
        }

        public static string FormatNumber(decimal aAmount, string aFormat)
        {
            return aAmount.ToString(aFormat);
        }

        public static string FormatNumber(object aAmount)
        {
            return FormatNumber(Convert.ToDecimal(aAmount));
        }

        public static string FormatNumber(object aAmount, string aFormat)
        {
            return FormatNumber(Convert.ToDecimal(aAmount), aFormat);
        }

        public static string FormatNumber(string aAmount)
        {
            return FormatNumber(Convert.ToDecimal(aAmount));
        }

        public static string FormatNumber(string aAmount, string aFormat)
        {
            return FormatNumber(Convert.ToDecimal(aAmount), aFormat);
        }

        public static decimal FormatDecimal(decimal aAmount, int aDecimalPlace)
        {
            return Math.Round(aAmount, aDecimalPlace);
        }
        #endregion   

        public static bool IsExpired()
        {
            //if (DateTime.Now >= new DateTime(2018, 7, 31))
            //{
            //    return true;
            //}
            //return false;
            return false;
        }

        public enum JsonStatus { OK, ERROR, NOTFOUND }
    }
}
