﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace buzz.backend.Models
{
    public class Template
    {
        public Guid? templateid { get; set; }
        public string createddate { get; set; }
        public string modifieddate { get; set; }
        public string templatename { get; set; }
        public string templateurl { get; set; }
        public string userid { get; set; }
        public byte recordstatus { get; set; }
    }
}
