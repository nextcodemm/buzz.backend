﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace buzz.backend.Models
{
    public class DoctorSchedule
    {
        public Guid? doctorscheduleid { get; set; }
        public byte recordstatus { get; set; }
        public string createddate { get; set; }
        public string modifieddate { get; set; }
        public string userid { get; set; }
        public Guid? doctorid { get; set; }
        public Guid? healthcareunitid { get; set; }
        public string day { get; set; }
        public string starttime { get; set; }
        public string endtime { get; set; }
        public Doctor doctor { get; set; }
    }
}
