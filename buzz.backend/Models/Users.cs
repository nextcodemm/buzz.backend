﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace buzz.backend.Models
{
    public class Users
    {
        public Guid? userid { get; set; }
        public Decimal reputation { get; set; }
        public string creationdate { get; set; }
        public string displayname { get; set; }
        public string lastaccessdate { get; set; }
        public string websiteurl { get; set; }
        public string location { get; set; }
        public string aboutme { get; set; }
        public int views { get; set; }
        public int upvotes { get; set; }
        public int downvotes { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public Guid? accountid { get; set; }
        public int age { get; set; }
        public int isdoctor { get; set; }
    }
}