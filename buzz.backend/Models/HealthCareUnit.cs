﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace buzz.backend.Models
{
    public class HealthCareUnit
    {
        public Guid? healthcareunitid { get; set; }
        public string createddate { get; set; }
        public string modifieddate { get; set; }
        public string healthcareunitname { get; set; }
        public Guid? unittypeid { get; set; }
        public string address { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public string contactperson { get; set; }
        public string servername { get; set; }
        public string databasename { get; set; }
        public string databaseuser { get; set; }
        public string databasepassword { get; set; }
        public string userid { get; set; }
        public byte recordstatus { get; set; }
        public HealthCareUnitType unitType { get; set; }
    }
}
