﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace buzz.backend.Models
{
    public class Comments
    {
        public Guid? commentid { get; set; }
        public Guid? postid { get; set; }
        public int score { get; set; }
        public string text { get; set; }
        public DateTime creationdate { get; set; }
        public string userdisplayname { get; set; }
        public Guid? userid { get; set; }
        public Posts Posts { get; set; }
    }
}