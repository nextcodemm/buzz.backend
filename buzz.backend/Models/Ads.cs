﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace buzz.backend.Models
{
    public class Ads
    {
        public Guid? adsid { get; set; }
        public Guid? adsgroupid { get; set; }
        public string createddate { get; set; }
        public string modifieddate { get; set; }
        public string adsname { get; set; }
        public string adscompany { get; set; }
        public string adssource { get; set; }
        public int durationinseconds { get; set; }
        public string userid { get; set; }
        public byte recordstatus { get; set; }
        public DisplayBoardSetting DisplayBoardSetting { get; set; }
    }

    public class AdsVM: Ads
    {
        public string adsfile { get; set; }
        public string adsfiletype { get; set; }
    }
}