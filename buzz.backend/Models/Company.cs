﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace buzz.backend.Models
{
    public class Company
    {
        public Guid? companyid { get; set; }
        public string createddate { get; set; }
        public string modifieddate { get; set; }
        public string companyname { get; set; }
        public string contact { get; set; }
        public string email { get; set; }
        public string website { get; set; }
        public string otherinformation { get; set; }
        public bool bdisplayonmain { get; set; }
        public string userid { get; set; }
        public byte recordstatus { get; set; }
    }
}

