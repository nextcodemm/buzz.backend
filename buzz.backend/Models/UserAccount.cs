﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace buzz.backend.Models
{
    public class UserAccount
    {
        public Guid? useraccountid { get; set; }
        public string createddate { get; set; }
        public string modifieddate { get; set; }
        public string username { get; set; }
        public string email { get; set; }
        public string mobile { get; set; }
        public string password { get; set; }
        public int accountstatus { get; set; }
        public string profilepicture { get; set; }
        public string verifycode { get; set; }
        public string verifystatus { get; set; }
        public string userid { get; set; }
        public byte recordstatus { get; set; }
        public IEnumerable<HealthCareUnitUser> healthCareUnitUsers { get; set; }
    }
}
