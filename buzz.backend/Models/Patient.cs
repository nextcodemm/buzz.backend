﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace buzz.backend.Models
{
    public class Patient
    {
        public long patientid { get; set; }
        public string patientreference { get; set; }
        public string createddate { get; set; }
        public string modifieddate { get; set; }
        public string patientname { get; set; }
        public string nrcnumber { get; set; }
        public string pastportnumber { get; set; }
        public string dob { get; set; }
        public string age { get; set; }
        public string gender { get; set; }
        public string bloodtype { get; set; }
        public string maritalstatus { get; set; }
        public string religion { get; set; }
        public string country { get; set; }
        public string state { get; set; }
        public string zipcode { get; set; }
        public string nationality { get; set; }
        public string address { get; set; }
        public string placeofbirth { get; set; }
        public string city { get; set; }
        public string postalcode { get; set; }
        public string fathername { get; set; }
        public string mothername { get; set; }
        public string occupation { get; set; }
        public string phone { get; set; }
        public string mobile { get; set; }
        public string fax { get; set; }
        public string emial { get; set; }
        public byte allergy { get; set; }
        public string remark { get; set; }
        public byte patientstatus { get; set; }
        public string userid { get; set; }
        public byte recordstatus { get; set; }
        public int counternumber { get; set; }
        public int stateid { get; set; }
        public Counter counter { get; set; }
    }
}