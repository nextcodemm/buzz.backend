﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Lucene.Net.Linq.Mapping;
using Lucene.Net.Linq.Search;

namespace buzz.backend.Models
{
    public class Doctor
    {
        public Guid? doctorid { get; set; }
        public byte recordstatus { get; set; }
        public string createddate { get; set; }
        public string modifieddate { get; set; }
        public string doctorname { get; set; }
        public Guid? categoryid { get; set; }
        public string speciality { get; set; }
        public string degree { get; set; }
        public string rank { get; set; }
        public string department { get; set; }
        public string licencenumber { get; set; }
        public string gender { get; set; }
        public string address { get; set; }
        public string homephone { get; set; }
        public string mobile { get; set; }
        public string email { get; set; }
        public string userid { get; set; }
        public string doctorsource { get; set; }
        public Category Category { get; set; }
        public string imagedata { get; set; }
        public Guid? useraccountid { get; set; }
        public Users Users { get; set; }
    }
}
