﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace buzz.backend.Models
{
    public class QueueStack
    {
        public Guid? queuestackid { get; set; }
        public string createddate { get; set; }
        public string modifieddate { get; set; }
        public string queuedate { get; set; }
        public string queuenumber { get; set; }
        public int countercategoryid { get; set; }
        public Guid? counterid { get; set; }
        public string queuestatus { get; set; }
        public int? queueagainorder { get; set; }
        public DateTime? settime { get; set; }
        public DateTime? reservetime { get; set; }
        public DateTime? servetime { get; set; }
        public DateTime? endtime { get; set; }
        public string userid { get; set; }
        public byte recordstatus { get; set; }
    }
}
