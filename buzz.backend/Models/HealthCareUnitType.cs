﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace buzz.backend.Models
{
    public class HealthCareUnitType
    {
        public Guid? unittypeid { get; set; }
        public string createddate { get; set; }
        public string modifieddate { get; set; }
        public string unittype { get; set; }
        public string userid { get; set; }
        public byte recordstatus { get; set; }
    }
}
