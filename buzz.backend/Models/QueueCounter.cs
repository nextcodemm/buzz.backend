﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace buzz.backend.Models
{
    public class QueueCounter
    {
        public Guid? queuecounterid { get; set; }
        public string createddate { get; set; }
        public string modifieddate { get; set; }
        public string countername { get; set; }
        public int? countercategoryid { get; set; }
        public int displayorder { get; set; }
        public string backcolor { get; set; }
        public string userid { get; set; }
        public byte recordstatus { get; set; }
    }
}
