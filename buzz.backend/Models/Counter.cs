﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace buzz.backend.Models
{
    public class Counter
    {
        public string createddate { get; set; }
        public string modifieddate { get; set; }
        public int counternumber { get; set; }
        public string countername { get; set; }
        public string computername { get; set; }
        public int locationid { get; set; }
        public int binid { get; set; }
        public string userid { get; set; }
        public byte recordstatus { get; set; }
    }
}