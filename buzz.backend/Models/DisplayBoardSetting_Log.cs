﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace buzz.backend.Models
{
    public class DisplayBoardSetting_Log
    {
        public Guid? settinglogid { get; set; }
        public Guid? settingid { get; set; }
        public string createddate { get; set; }
        public string modifieddate { get; set; }
        public Guid? displayboardid { get; set; }
        public Guid? doctorid { get; set; }
        public bool notshowad { get; set; }
        public bool notshowtime { get; set; }
        public DateTime? systemstarttime { get; set; }
        public DateTime? systemendtime { get; set; }
        public string starttime { get; set; }
        public string endtime { get; set; }
        public string userid { get; set; }
        public byte recordstatus { get; set; }
        public DisplayBoard DisplayBoard { get; set; }
        public Doctor Doctor { get; set; }
    }
}
