﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace buzz.backend.Models
{
    public class HealthCareUnitUser
    {
        public Guid? healthcareunituserid { get; set; }
        public Guid? healthcareunitid { get; set; }
        public Guid? useraccountid { get; set; }
        public string createddate { get; set; }
        public string modifieddate { get; set; }
        public string userid { get; set; }
        public byte recordstatus { get; set; }
    }
}
