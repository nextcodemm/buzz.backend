﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace buzz.backend.Models
{
    public class DisplayBoard
    {
        public Guid? displayboardid { get; set; }
        public string createddate { get; set; }
        public string modifieddate { get; set; }
        public string displayboardname { get; set; }
        public Guid? groupid { get; set; }
        public Guid? mainboardid { get; set; }
        public Guid? templateid { get; set; }
        public int displayorder { get; set; }
        public byte status { get; set; }
        public string userid { get; set; }
        public byte recordstatus { get; set; }
        public DisplayGroup DisplayGroup { get; set; }
        public DisplayBoardSetting DisplayBoardSetting { get; set; }
        public Doctor Doctor { get; set; }
        public Template Template { get; set; }
        public AdsSetting AdsSetting { get; set; }

    }
}
