﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace buzz.backend.Models
{
    public class AdsSetting
    {
        public Guid? adssettingid { get; set; }
        public string createddate { get; set; }
        public string modifieddate { get; set; }
        public Guid? adsgroupid { get; set; }
        public Guid? displayboardid { get; set; }
        public string userid { get; set; }
        public byte recordstatus { get; set; }
        public DisplayBoard DisplayBoard { get; set; }
    }
}