﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace buzz.backend.Models
{
    public class Posts
    {
        public Guid? postid { get; set; }
        public int posttypeid { get; set; }
        public Guid? acceptedanswerid { get; set; }
        public Guid? parentid { get; set; }
        public DateTime creationdate { get; set; }
        public int score { get; set; }
        public int viewcount { get; set; }
        public string body { get; set; }
        public Guid? owneruserid { get; set; }
        public string ownerdisplayname { get; set; }
        public Guid? lasteditoruserid { get; set; }
        public string lasteditordisplayname { get; set; }
        public string lasteditdate { get; set; }
        public string lastactivitydate { get; set; }
        public string title { get; set; }
        public string tags { get; set; }
        public int answercount { get; set; }
        public int commentcount { get; set; }
        public int favoritecount { get; set; }
        public string closeddate { get; set; }
        public string communityowneddate { get; set; }

        public long creationdateInMillionSecond { get; set; }
    }
}