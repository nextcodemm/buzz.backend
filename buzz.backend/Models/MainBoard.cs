﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace buzz.backend.Models
{
    public class MainBoard
    {
        public Guid? displayboardid { get; set; }
        public string displayboardname { get; set; }
        public Guid? doctorid { get; set; }
        public string doctorname { get; set; }
        public string gender { get; set; }
        public string speciality { get; set; }
        public string degree { get; set; }
        public string department { get; set; }
        public string doctorsource { get; set; }
        public string imagedata { get; set; }
    }
}
