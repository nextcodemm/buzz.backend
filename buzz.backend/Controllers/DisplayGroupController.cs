﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Cors;
using Dapper;
using System.Web.Mvc;
using buzz.backend.Utilities;
using buzz.backend.Models;
using System.Net;

namespace buzz.backend.Controllers
{
    //[Produces("application/json")]
    [Route("api/displaygroup")]
    public class DisplayGroupController : Controller
    {
        [HttpGet]
        [EnableCors("MyPolicy")]
        public IEnumerable<DisplayGroup> Get()
        {
            using (var db = My.ConnectionFactory())
            {
                return db.Query<DisplayGroup>(My.Table_DisplayGroup.Select).ToList();
            }
        }

        [Route("api/displaygroup/{id}")]
        [HttpGet()]
        [EnableCors("MyPolicy")]
        public DisplayGroup Get(string id)
        {
            using (var db = My.ConnectionFactory())
            {
                return db.QuerySingle<DisplayGroup>(My.Table_DisplayGroup.SelectSingle, new { groupid = id });
            }
        }

        [HttpPost]
        [EnableCors("MyPolicy")]
        public ActionResult Save(DisplayGroup value)
        {
            try
            {
                using (var db = My.ConnectionFactory())
                {
                    if (value.groupid == null)
                    {
                        value.groupid = Guid.NewGuid();
                    }
                    value.createddate = (value.createddate == null || value.createddate.Equals("")) ? AppUtilities.DateToString() : value.createddate;
                    value.modifieddate = AppUtilities.DateToString();
                    value.userid = "thz";
                    value.recordstatus = 1;
                    int result = db.Execute($@"IF EXISTS({My.Table_DisplayGroup.SelectSingle}) {My.Table_DisplayGroup.Update} ELSE {My.Table_DisplayGroup.Insert}", value);
                }
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete()]
        [EnableCors("MyPolicy")]
        public ActionResult Delete(string id)
        {
            try
            {
                using (var db = My.ConnectionFactory())
                {
                    int result = db.Execute(My.Table_DisplayGroup.Delete, new { groupid = id });
                    if (result > 0) return Ok(); else return NotFound();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        
        private ActionResult BadRequest(string message)
        {
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest, message);
        }

        private ActionResult NotFound()
        {
            return new HttpStatusCodeResult(HttpStatusCode.NotFound);
        }

        private ActionResult Ok()
        {
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
    }
}
