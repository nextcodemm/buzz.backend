﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Cors;
using Dapper;
using System.Web.Mvc;
using buzz.backend.Utilities;
using buzz.backend.Models;
using System.Net;
using System.Web;
using System.IO;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using System.Web.Script.Serialization;

namespace buzz.backend.Controllers
{
    public class CommentController : Controller
    {
        protected override JsonResult Json(object data, string contentType, System.Text.Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new JsonResult()
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior,
                MaxJsonLength = Int32.MaxValue
            };
        }

        [Route("api/comments/{postid}")]
        [HttpGet]
        [EnableCors("MyPolicy")]
        public JsonResult Get(string postid)
        {
            using (var db = My.ConnectionFactory())
            {
                //IEnumerable<Doctor> doctors = db.Query<Doctor, Category, Doctor>(sql, (idoctor, icategory) =>
                //{
                //    idoctor.Category = icategory;
                //    return idoctor;
                //}, new { from = ((current - 1) * size), to = ((current - 1) * size) + size }, splitOn: "Id");

                string sql = "SELECT C.*,P.postid AS Id , P.*" +
                    " FROM Comments C JOIN POSTS P ON C.postid = P.postid "
                        + "WHERE C.postid = @postid " +
                        "ORDER BY C.creationdate";

                IEnumerable<Comments> comments = db.Query<Comments,Posts,Comments>(sql, (icomments, iposts) =>
                {
                    icomments.Posts = iposts;
                    return icomments;
                }, new { postid = postid },splitOn : "Id");

                return Json(new { comments }, JsonRequestBehavior.AllowGet);
            }
        }

        [Route("api/comments/save")]
        [HttpPost]
        [EnableCors("MyPolicy")]
        public ActionResult Save(Comments comment)
        {
            try
            {
                using (var db = My.ConnectionFactory())
                {
                    UserController userController = new UserController();

                    if (comment.commentid == null)
                    {
                       
                        string sql = "UPDATE Posts SET commentcount = commentcount + 1 " +
                                        " Where postid = @postid";
                        int res = db.Execute(sql, new { postid = comment.postid });
                        
                        comment.commentid = Guid.NewGuid();
                        comment.creationdate = DateTime.Now;
                    }

                    //post.creationdate = (post.creationdate == null || post.creationdate.Equals("")) ? AppUtilities.DateToString() : post.creationdate;
                    int result = db.Execute($@"IF EXISTS({My.Table_Comments.SelectSingle}) {My.Table_Comments.Update} ELSE {My.Table_Comments.Insert}", comment);
                }
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Route("api/comments/delete/{commentid}")]
        [HttpDelete()]
        [EnableCors("MyPolicy")]
        public ActionResult Delete(string commentid)
        {
            try
            {
                try
                {
                    using (var db = My.ConnectionFactory())
                    {
                        string sql = "Delete From Comments Where commentid = @commentid";
                        int result = db.Execute(sql, new { commentid = commentid });
                    }
                    return Ok();
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


        private ActionResult BadRequest(string message)
        {
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest, message);
        }

        private ActionResult NotFound()
        {
            return new HttpStatusCodeResult(HttpStatusCode.NotFound);
        }

        private ActionResult Ok()
        {
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }


    }
}