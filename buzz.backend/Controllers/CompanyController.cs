﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Cors;
using Dapper;
using System.Web.Mvc;
using buzz.backend.Utilities;
using buzz.backend.Models;
using System.Net;

namespace buzz.backend.Controllers
{
    //[Produces("application/json")]
    [Route("api/companies")]
    public class CompanyController : Controller
    {
        [HttpGet]
        [EnableCors("MyPolicy")]
        public JsonResult Get()
        {
            using (var db = My.ConnectionFactory())
            {

                string sqlDate = "SELECT GETDATE()";
                String currentdate = db.ExecuteScalar<String>(sqlDate);

                return Json(new { companies = db.Query<Company>(My.Table_Company.Select).ToList(), currentdate = currentdate  }, JsonRequestBehavior.AllowGet);
            }
        }

        [Route("api/companies/{id}")]
        [HttpGet]
        [EnableCors("MyPolicy")]
        public JsonResult Get(string id)
        {
            using (var db = My.ConnectionFactory())
            {
                var company = db.QuerySingle<Company>(My.Table_Company.SelectSingle, new { companyid = id });
                return Json(new { company = company }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [EnableCors("MyPolicy")]
        public ActionResult Save(Company company)
        {
            try
            {
                using (var db = My.ConnectionFactory())
                {
                    if (company.companyid == null)
                    {
                        company.companyid = Guid.NewGuid();
                    }
                    company.createddate = (company.createddate == null || company.createddate.Equals("")) ? AppUtilities.DateToString() : company.createddate;
                    company.modifieddate = AppUtilities.DateToString();
                    company.userid = "thz";
                    company.recordstatus = 1;
                    int result = db.Execute($@"IF EXISTS({My.Table_Company.SelectSingle}) {My.Table_Company.Update} ELSE {My.Table_Company.Insert}", company);
                }
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Route("api/companies/{companyid}")]
        [HttpDelete]
        [EnableCors("MyPolicy")]
        public ActionResult Delete(string companyid)
        {
            try
            {
                using (var db = My.ConnectionFactory())
                {
                    int result = db.Execute(My.Table_Company.Delete, new { companyid = companyid });
                    if (result > 0) return Ok(); else return NotFound();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        private ActionResult BadRequest(string message)
        {
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest, message);
        }

        private ActionResult NotFound()
        {
            return new HttpStatusCodeResult(HttpStatusCode.NotFound);
        }

        private ActionResult Ok()
        {
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
    }
}
