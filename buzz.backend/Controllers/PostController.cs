﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Cors;
using Dapper;
using System.Web.Mvc;
using buzz.backend.Utilities;
using buzz.backend.Models;
using System.Net;
using System.Web;
using System.IO;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using System.Web.Script.Serialization;

namespace buzz.backend.Controllers
{
    public class PostController : Controller
    {
        protected override JsonResult Json(object data, string contentType, System.Text.Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new JsonResult()
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior,
                MaxJsonLength = Int32.MaxValue
            };
        }

        [Route("api/posts/getallposts/{posttypeid}")]
        [HttpGet]
        [EnableCors("MyPolicy")]
        public JsonResult Get(string posttypeid)
        {
            using (var db = My.ConnectionFactory())
            {
                string sql = "SELECT * FROM Posts "
                        +"WHERE posttypeid = @posttypeid" +
                        " ORDER BY creationdate DESC";
              
                IEnumerable<Posts> posts = db.Query<Posts>(sql,new { posttypeid = posttypeid });

                foreach (var item in posts)
                {
                    item.creationdateInMillionSecond = long.Parse(item.creationdate.ToString("yyyyMMddHHmmss"));

                }

                return Json(new { posts }, JsonRequestBehavior.AllowGet);
            }
        }

        [Route("api/posts/getpostbypostid/{postid}")]
        [HttpGet()]
        [EnableCors("MyPolicy")]
        public JsonResult GetByPostid(string postid)
        {
            using (var db = My.ConnectionFactory())
            {
                string sql = "SELECT * FROM Posts WHERE postid = @postid";

               Posts posts = db.Query<Posts>(sql, new { postid = postid }).FirstOrDefault();

                return Json(new { posts }, JsonRequestBehavior.AllowGet);

            }
        }

        [Route("api/posts/save")]
        [HttpPost]
        [EnableCors("MyPolicy")]
        public ActionResult Save(Posts post)
        {
            try
            {
                using (var db = My.ConnectionFactory())
                {
                    UserController userController = new UserController();

                    if (post.postid == null)
                    {
                        if (post.posttypeid == 2)
                        {
                            string sql = "UPDATE Posts SET answercount = answercount + 1 " +
                                         " Where postid = @parentid";
                            int res = db.Execute(sql, new { parentid = post.parentid });
                        }
                        post.postid = Guid.NewGuid();
                        post.creationdate = DateTime.Now;
                    }

                    //post.creationdate = (post.creationdate == null || post.creationdate.Equals("")) ? AppUtilities.DateToString() : post.creationdate;
                    int result = db.Execute($@"IF EXISTS({My.Table_Posts.SelectSingle}) {My.Table_Posts.Update} ELSE {My.Table_Posts.Insert}", post);
                }
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Route("api/posts/delete/{postid}")]
        [HttpDelete()]
        [EnableCors("MyPolicy")]
        public ActionResult Delete(string postid)
        {
            try
            {
                try
                {
                    using (var db = My.ConnectionFactory())
                    {
                        string sql = "Delete From Posts Where postid = @postid";
                        int result = db.Execute(sql, new { postid = postid });
                    }
                    return Ok();
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Route("api/posts/{posttypeid}/{current}/{size}")]
        [HttpGet]
        [EnableCors("MyPolicy")]
        public JsonResult GetByPage(int current, int size,int posttypeid)
        {
            using (var db = My.ConnectionFactory())
            {
                string sql = "SELECT  * "
                    + " FROM(SELECT    ROW_NUMBER() OVER(ORDER BY creationdate DESC) AS RowNum, * "
                    + "           FROM      Posts "
                    + "         ) AS P"
                    + " WHERE RowNum >= @from "
                    + "     AND RowNum< @to "
                    + "     AND posttypeid = @posttypeid" 
                    + " ORDER BY RowNum";

                IEnumerable<Posts> posts = db.Query<Posts>(sql, new { from = ((current - 1) * size), to = ((current - 1) * size) + size, posttypeid = posttypeid });

                foreach (var item in posts)
                {
                    item.creationdateInMillionSecond = long.Parse(item.creationdate.ToString("yyyyMMddHHmmss"));

                }

                return Json(new { posts }, JsonRequestBehavior.AllowGet);
            }
        }

        [Route("api/posts/getallanswers/{parentid}")]
        [HttpGet]
        [EnableCors("MyPolicy")]
        public JsonResult GetAllAnswers(string parentid)
        {
            using (var db = My.ConnectionFactory())
            {
                string sql = "SELECT * FROM Posts "
                        + "WHERE parentid = @parentid ORDER BY creationdate DESC";

                IEnumerable<Posts> posts = db.Query<Posts>(sql, new { parentid = parentid });

                foreach (var item in posts)
                {
                    item.creationdateInMillionSecond = long.Parse(item.creationdate.ToString("yyyyMMddHHmmss"));

                }

                return Json(new { posts }, JsonRequestBehavior.AllowGet);
            }
        }

        [Route("api/posts/upvotes/{owneruserid}/{postid}/{userid}")]
        [HttpGet]
        [EnableCors("MyPolicy")]
        public ActionResult Getvotesup(string postid,string userid, string owneruserid)
        {
            try
            {
                using (var db = My.ConnectionFactory())
                {
                    string sql = "UPDATE Posts SET score = score + 1 " +
                        " Where postid = @postid";
                    int result = db.Execute(sql, new { postid = postid });

                    string sql1 = "UPDATE Users SET upvotes = upvotes + 1 " +
                        " Where userid = @userid";
                    int result1 = db.Execute(sql1, new { userid = userid });

                    string sql2 = "UPDATE Users SET reputation = reputation + 1 " +
                        " Where userid = @userid";
                    int result2 = db.Execute(sql2, new { userid = owneruserid });
                }
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Route("api/posts/downvotes/{owneruserid}/{postid}/{userid}")]
        [HttpGet]
        [EnableCors("MyPolicy")]
        public ActionResult Getvotedown(string postid, string userid, string owneruserid)
        {
            try
            {
                using (var db = My.ConnectionFactory())
                {
                    string sql = "UPDATE Posts SET score = score - 1 " +
                        " Where postid = @postid";
                    int result = db.Execute(sql, new { postid = postid });

                    string sql1 = "UPDATE Users SET downvotes = downvotes + 1 " +
                        " Where userid = @userid";
                    int result1 = db.Execute(sql1, new { userid = userid });

                    string sql2 = "UPDATE Users SET reputation = reputation - 1 " +
                        " Where userid = @userid";
                    int result2 = db.Execute(sql2, new { userid = owneruserid });
                }
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Route("api/posts/upvotes/{owneruserid}/{postid}/{userid}")]
        [HttpPost()]
        [EnableCors("MyPolicy")]
        public ActionResult voteUpPost(string postid, string userid, string owneruserid)
        {
            try
            {
                using (var db = My.ConnectionFactory())
                {
                    string sql = "UPDATE Posts SET score = score + 1 " +
                        " Where postid = @postid";
                    int result = db.Execute(sql, new { postid = postid });

                    string sql1 = "UPDATE Users SET upvotes = upvotes + 1 " +
                        " Where userid = @userid";
                    int result1 = db.Execute(sql1, new { userid = userid });

                    string sql2 = "UPDATE Users SET reputation = reputation + 1 " +
                        " Where userid = @userid";
                    int result2 = db.Execute(sql2, new { userid = owneruserid });
                }
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Route("api/posts/downvotes/{owneruserid}/{postid}/{userid}")]
        [HttpPost()]
        [EnableCors("MyPolicy")]
        public ActionResult voteDownPost(string postid, string userid, string owneruserid)
        {
            try
            {
                using (var db = My.ConnectionFactory())
                {
                    string sql = "UPDATE Posts SET score = score - 1 " +
                        " Where postid = @postid";
                    int result = db.Execute(sql, new { postid = postid });

                    string sql1 = "UPDATE Users SET downvotes = downvotes + 1 " +
                        " Where userid = @userid";
                    int result1 = db.Execute(sql1, new { userid = userid });

                    string sql2 = "UPDATE Users SET reputation = reputation - 1 " +
                        " Where userid = @userid";
                    int result2 = db.Execute(sql2, new { userid = owneruserid });
                }
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Route("api/posts/rightAnswer/{postid}/{parentid}/{owneruserid}")]
        [HttpPost()]
        [EnableCors("MyPolicy")]
        public ActionResult rightAnswer(string postid, string parentid, string owneruserid)
        {
            try
            {
                using (var db = My.ConnectionFactory())
                {
                    string sql = "UPDATE Posts SET acceptedanswerid = @postid, closeddate = CONVERT(NVARCHAR(50),GETDATE(),112) " +
                        " Where postid = @parentid";
                    int result = db.Execute(sql, new { postid = postid ,parentid = parentid});

                    string sql2 = "UPDATE Users SET reputation = reputation + 5 " +
                        " Where userid = @owneruserid";
                    int result2 = db.Execute(sql2, new { owneruserid = owneruserid });
                }
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


        [Route("api/getAllTotalsByToday")]
        [HttpGet]
        [EnableCors("MyPolicy")]
        public JsonResult GetAllTotals()
        {
            using (var db = My.ConnectionFactory())
            {
                string sql = "SELECT COUNT(*) FROM	Posts" +
                    " WHERE	CONVERT(NVARCHAR(50),creationdate,112) = CONVERT(NVARCHAR(50),GETDATE(),112) " +
                    " AND POSTTYPEID = 1";
                var totalquestion = db.Query<int>(sql);

                string sql2 = "SELECT COUNT(*) FROM	Posts" +
                    " WHERE	CONVERT(NVARCHAR(50),creationdate,112) = CONVERT(NVARCHAR(50),GETDATE(),112) " +
                    " AND POSTTYPEID = 2";
                var totalanswer = db.Query<int>(sql2);

                string sql3 = "SELECT COUNT(*) FROM	Posts" +
                    " WHERE	CONVERT(NVARCHAR(50),closeddate,112) = CONVERT(NVARCHAR(50),GETDATE(),112) ";
                var totalacceptedanswer = db.Query<int>(sql3);

                string sql4 = "SELECT owneruserid, count(postid) FROM Posts " +
                    "WHERE CONVERT(nvarchar(50),creationdate,112) = CONVERT(nvarchar(50), GETDATE(), 112) " +
                    "GROUP BY owneruserid";
                var totalactiveusers = db.Query<Object>(sql4).Count();

                var obj = new { totalquestion = totalquestion,totalanswer = totalanswer , totalacceptedanswer = totalacceptedanswer, totalactiveusers};
                
                return Json(new { obj }, JsonRequestBehavior.AllowGet);
            }
        }

        [Route("api/posts/getallquestionbyuserid/{userid}")]
        [HttpGet]
        [EnableCors("MyPolicy")]
        public JsonResult GetAllQuestionByUserid(string userid)
        {
            using (var db = My.ConnectionFactory())
            {
                string sql = "SELECT * FROM Posts "
                        + "WHERE owneruserid = @userid " +
                        " AND posttypeid = 1 ORDER BY creationdate DESC";

                IEnumerable<Posts> posts = db.Query<Posts>(sql, new { userid = userid });

                foreach (var item in posts)
                {
                    item.creationdateInMillionSecond = long.Parse(item.creationdate.ToString("yyyyMMddHHmmss"));

                }

                return Json(new { posts }, JsonRequestBehavior.AllowGet);
            }
        }

        private ActionResult BadRequest(string message)
        {
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest, message);
        }

        private ActionResult NotFound()
        {
            return new HttpStatusCodeResult(HttpStatusCode.NotFound);
        }

        private ActionResult Ok()
        {
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }


    }
}