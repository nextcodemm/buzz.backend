﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Cors;
using Dapper;
using System.Web.Mvc;
using buzz.backend.Utilities;
using buzz.backend.Models;
using System.Net;
using System.IO;
using System.Web;

namespace buzz.backend.Controllers
{
    //[Produces("application/json")]
    [Route("api/displayboardsetting")]
    public class DisplayBoardSettingController : Controller
    {
        protected override JsonResult Json(object data, string contentType, System.Text.Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new JsonResult()
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior,
                MaxJsonLength = Int32.MaxValue
            };
        }

        [HttpGet]
        public JsonResult Get()
        {
            using (var db = My.ConnectionFactory())
            {
                return Json(new { DisplayBoardSetting = db.Query<DisplayBoardSetting>(My.Table_DisplayBoardSetting.Select).ToList(), JsonRequestBehavior.AllowGet });
            }
        }

        [Route("api/displayboardsetting/displayboardid/{id}")]
        [HttpGet]
        public JsonResult GetByDisplayBoardId(string id)
        {
            if (AppUtilities.IsExpired())
            {
                return Json(new { displayboardsetting = "expired" }, JsonRequestBehavior.AllowGet);
            }
            using (var db = My.ConnectionFactory())
            {
                string sql = "SELECT DS.*, D.displayboardid AS id, D.*, Doc.doctorid AS id, Doc.* FROM DisplayBoardSetting AS DS LEFT JOIN DisplayBoard AS D " +
                    " ON DS.displayboardid = D.displayboardid" +
                    " LEFT JOIN Doctor AS Doc ON DS.doctorid = Doc.doctorid" +
                    " WHERE DS.recordstatus<> 2 AND DS.displayboardid = @displayboardid";

                DisplayBoardSetting displayboardsetting = db.Query<DisplayBoardSetting, DisplayBoard, Doctor, DisplayBoardSetting>(sql, (dboardSetting, displayboard, doctor) =>
                {
                    dboardSetting.DisplayBoard = displayboard;
                    dboardSetting.Doctor = doctor;
                    return dboardSetting;
                }, new { displayboardid = id }, splitOn: "Id").FirstOrDefault();

                return Json(new { displayboardsetting }, JsonRequestBehavior.AllowGet);

                //DisplayBoardSetting displayboardsetting = db.Query<DisplayBoardSetting>(sql, new { displayboardid = id }).SingleOrDefault();
                //return Json(new { displayboardsetting }, JsonRequestBehavior.AllowGet);
            }
        }

        [Route("api/displayboardsetting/mainboardid/{id}")]
        [HttpGet]
        public JsonResult GetByMainBoardId(string id)
        {
            if(AppUtilities.IsExpired())
            {
                return Json(new { mainBoardList = "expired" }, JsonRequestBehavior.AllowGet);
            }
            using (var db = My.ConnectionFactory())
            {
                string sql = "SELECT DB.displayboardid, DB.displayboardname, DBS.doctorid,DT.doctorname, DT.gender, DT.speciality, DT.degree, DT.department, DT.doctorsource" +
                    " FROM DisplayBoard AS DB " +
                    " INNER JOIN DisplayBoardSetting AS DBS " +
                    " ON DB.displayboardid = DBS.displayboardid " +
                    " INNER JOIN Doctor AS DT " +
                    " ON DBS.doctorid = DT.doctorid " +
                    " WHERE DB.recordstatus<>2 AND DB.mainboardid = @mainboardid " +
                    " AND DB.displayboardid<> DB.mainboardid " +
                    " ORDER BY DB.displayorder";
                IEnumerable<MainBoard> mainBoardList = db.Query<MainBoard>(sql, new { mainboardid = id }).ToList();

                for (int i = 0; i < mainBoardList.Count(); i++)
                {
                    string path = mainBoardList.ElementAt(i).doctorsource;
                    if (!string.IsNullOrEmpty(path) && System.IO.File.Exists(path))
                    {
                        byte[] imageByteData = System.IO.File.ReadAllBytes(path);
                        string imageBase64Data = Convert.ToBase64String(imageByteData);
                        string imageDataURL = string.Format("data:image/png;base64,{0}", imageBase64Data);
                        mainBoardList.ElementAt(i).imagedata = imageDataURL;
                    }
                }

                return Json(new { mainBoardList }, JsonRequestBehavior.AllowGet);
            }
        }

        [Route("api/displayboardsetting/{id}")]
        [HttpGet()]
        //[EnableCors("MyPolicy")]
        public DisplayBoardSetting Get(Guid? id)
        {
            using (var db = My.ConnectionFactory())
            {
                return db.QuerySingle<DisplayBoardSetting>(My.Table_DisplayBoardSetting.SelectSingle, new { settingid = id });
            }
        }

        [Route("api/displayboardsetting/doctorsearch/{search}")]
        [HttpGet]
        public JsonResult DoctorSearch(string search)
        {
            using (var db = My.ConnectionFactory())
            {
                string sql = "SELECT DBS.*, DB.displayboardid AS Id, DB.*, D.doctorid AS Id, D.*, C.categoryid AS Id, C.* FROM DisplayBoardSetting AS DBS "
                    + "                     INNER JOIN DisplayBoard AS DB "
                    + "                     ON DBS.displayboardid = DB.displayboardid "
                    + "                     INNER JOIN Doctor AS D "
                    + "                     ON DBS.doctorid = D.doctorid ";
                if (!string.IsNullOrEmpty(search) && search != "''")
                {
                    sql += "                     INNER JOIN "
                    + "                     FREETEXTTABLE(Doctor, doctorname, "
                    + "                    @search) AS KEY_TBL"
                    + "                    ON D.doctorid = KEY_TBL.[KEY] ";
                }

                sql += "                     LEFT JOIN Category AS C "
                + "                     ON D.categoryid = C.categoryid"
                + " WHERE DB.recordstatus<>2";

                IEnumerable<DisplayBoardSetting> displayboardsettingraw = db.Query<DisplayBoardSetting, DisplayBoard, Doctor, Category, DisplayBoardSetting>(sql, (dboardSetting, displayboard, doctor, category) =>
               {
                   dboardSetting.DisplayBoard = displayboard;
                   dboardSetting.Doctor = doctor;
                   dboardSetting.Doctor.Category = category;
                   return dboardSetting;
               }, new { search = search }, splitOn: "Id");


                IEnumerable<DisplayBoardSetting> displayboardsettings = displayboardsettingraw;



                return Json(new { displayboardsettings }, JsonRequestBehavior.AllowGet);

                //DisplayBoardSetting displayboardsetting = db.Query<DisplayBoardSetting>(sql, new { displayboardid = id }).SingleOrDefault();
                //return Json(new { displayboardsetting }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        //[EnableCors("MyPolicy")]
        public ActionResult Save(DisplayBoardSetting template)
        {
            try
            {
                using (var db = My.ConnectionFactory())
                {
                    if (template.settingid == null)
                    {
                        template.settingid = Guid.NewGuid();
                    }
                    else
                    {
                        //Set System End Time to Previous
                        DisplayBoardSetting prevDisplayBoardSetting = db.QuerySingle<DisplayBoardSetting>(My.Table_DisplayBoardSetting.SelectSingle, new { settingid = template.settingid });
                        if (prevDisplayBoardSetting != null)
                        {
                            prevDisplayBoardSetting.systemendtime = DateTime.Now;
                            db.Execute(My.Table_DisplayBoardSetting.Update, prevDisplayBoardSetting);

                            //SaveToLog
                            SaveLog(prevDisplayBoardSetting);
                        }
                    }
                    template.createddate = (template.createddate == null || template.createddate.Equals("")) ? AppUtilities.DateToString() : template.createddate;
                    template.modifieddate = AppUtilities.DateToString();
                    template.systemstarttime = DateTime.Now;
                    template.userid = "thz";
                    template.recordstatus = 1;
                    int result = db.Execute($@"IF EXISTS({My.Table_DisplayBoardSetting.SelectSingle}) {My.Table_DisplayBoardSetting.Update} ELSE {My.Table_DisplayBoardSetting.Insert}", template);

                }
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        public ActionResult SaveLog(DisplayBoardSetting displayBoardSetting)
        {
            try
            {
                using (var db = My.ConnectionFactory())
                {
                    var template = new DisplayBoardSetting_Log();
                    template.settinglogid = Guid.NewGuid();
                    template.settingid = displayBoardSetting.settingid;
                    template.createddate =  AppUtilities.DateToString();
                    template.modifieddate = AppUtilities.DateToString();
                    template.displayboardid = displayBoardSetting.displayboardid;
                    template.doctorid = displayBoardSetting.doctorid;
                    template.notshowtime = displayBoardSetting.notshowtime;
                    template.notshowad = displayBoardSetting.notshowad;
                    template.systemstarttime = displayBoardSetting.systemstarttime;
                    template.systemendtime = displayBoardSetting.systemendtime;
                    template.starttime = displayBoardSetting.starttime;
                    template.endtime = displayBoardSetting.endtime;
                    template.userid = "thz";
                    template.recordstatus = 1;
                    int result = db.Execute($@"{My.Table_DisplayBoardSetting_Log.Insert}", template);
                }
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete()]
        //[EnableCors("MyPolicy")]
        public ActionResult Delete(string id)
        {
            try
            {
                using (var db = My.ConnectionFactory())
                {
                    int result = db.Execute(My.Table_DisplayBoardSetting.Delete, new { settingid = id });
                    if (result > 0) return Ok(); else return NotFound();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        private ActionResult BadRequest(string message)
        {
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest, message);
        }

        private ActionResult NotFound()
        {
            return new HttpStatusCodeResult(HttpStatusCode.NotFound);
        }

        private ActionResult Ok()
        {
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
    }
}
