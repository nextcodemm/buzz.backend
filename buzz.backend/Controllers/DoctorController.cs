﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Cors;
using Dapper;
using System.Web.Mvc;
using buzz.backend.Utilities;
using buzz.backend.Models;
using System.Net;
using System.Web;
using System.IO;
using System.Web.Http;


namespace buzz.backend.Controllers
{
    //[Produces("application/json")]
    //[Authorize]

    [RoutePrefix("api/doctors")]
    //[Route("api/doctors")]
    public class DoctorController : Controller
    {
        protected override JsonResult Json(object data, string contentType, System.Text.Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new JsonResult()
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior,
                MaxJsonLength = Int32.MaxValue
            };
        }

        [Route("")]
        [HttpGet]
        [EnableCors("MyPolicy")]
        public JsonResult Get()
        {
            using (var db = My.ConnectionFactory())
            {
                string sql = "SELECT D.*, C.categoryid AS Id, C.* FROM Doctor AS D "
                    + " LEFT JOIN Category AS C "
                    + " ON D.categoryid = C.categoryid"
                    + " WHERE D.recordstatus<> 2";

                IEnumerable<Doctor> doctors = db.Query<Doctor, Category, Doctor>(sql, (idoctor, icategory) =>
                {
                    idoctor.Category = icategory;
                    return idoctor;
                }, splitOn: "Id");

                return Json(new { doctors }, JsonRequestBehavior.AllowGet);
            }
        }

        [Route("totalcount")]
        [HttpGet()]
        [EnableCors("MyPolicy")]
        public JsonResult GetTotalCount()
        {
            using (var db = My.ConnectionFactory())
            {
                string sql = "SELECT COUNT(*) FROM Doctor AS D "
                    + " WHERE D.recordstatus<> 2 ";

                int count = db.Query<int>(sql).FirstOrDefault();

                return Json(new { count }, JsonRequestBehavior.AllowGet);

                //return Json(new { doctor = db.QuerySingle<Doctor>(My.Table_Doctor.SelectSingle, new { doctorId = doctorid }) }, JsonRequestBehavior.AllowGet);
            }
        }

        [Route("totalcount/{categoryid}")]
        [HttpGet()]
        [EnableCors("MyPolicy")]
        public JsonResult GetTotalCount(string categoryid)
        {
            using (var db = My.ConnectionFactory())
            {
                string sql = "SELECT COUNT(*) FROM Doctor AS D "
                    + " WHERE D.recordstatus<> 2 ";
                if(categoryid != "All")
                {
                    sql += " AND categoryid = @categoryid";
                }

                int count = db.Query<int>(sql, new { categoryid = categoryid }).FirstOrDefault();

                return Json(new { count }, JsonRequestBehavior.AllowGet);

                //return Json(new { doctor = db.QuerySingle<Doctor>(My.Table_Doctor.SelectSingle, new { doctorId = doctorid }) }, JsonRequestBehavior.AllowGet);
            }
        }

        [Route("totalcount/{categoryid}/{doctorname}")]
        [HttpGet()]
        [EnableCors("MyPolicy")]
        public JsonResult GetTotalCount(string categoryid, string doctorname)
        {
            using (var db = My.ConnectionFactory())
            {
                string sql = "SELECT COUNT(*) FROM Doctor AS D "
                    + " WHERE D.recordstatus<> 2 ";
                if (categoryid != "All")
                {
                    sql += " AND categoryid = @categoryid";
                }
                if (!string.IsNullOrEmpty(doctorname))
                {
                    sql += " AND doctorname LIKE @doctorname";
                }

                int count = db.Query<int>(sql, new { categoryid = categoryid, doctorname = "%" + doctorname + "%" }).FirstOrDefault();

                return Json(new { count }, JsonRequestBehavior.AllowGet);

                //return Json(new { doctor = db.QuerySingle<Doctor>(My.Table_Doctor.SelectSingle, new { doctorId = doctorid }) }, JsonRequestBehavior.AllowGet);
            }
        }

        [Route("getAllDoctors/{current}/{size}")]
        [HttpGet]
        [EnableCors("MyPolicy")]
        public JsonResult GetByPage(int current, int size)
        {
            using (var db = My.ConnectionFactory())
            {
                string sql = "SELECT  D.*, C.categoryid AS Id, C.*, U.userid AS Id , U.* "
                    + " FROM(SELECT    ROW_NUMBER() OVER(ORDER BY DC.doctorname) AS RowNum, DC.* "
                    + "           FROM      Doctor AS DC " +
                                 "JOIN     Users   AS U ON DC.useraccountid = U.userid              "
                    + "           WHERE     DC.recordstatus <> 2 "
                    + "         ) AS D "
                    + " LEFT JOIN Category AS C "
                    + " ON D.categoryid = C.categoryid" +
                    "   JOIN USERS AS U ON U.userid = D.useraccountid  "

                    + " WHERE RowNum >= @from "
                    + "     AND RowNum< @to "
                    + " ORDER BY RowNum";

                IEnumerable<Doctor> doctors = db.Query<Doctor, Category, Users,Doctor>(sql, (idoctor, icategory, iusers) =>
                {
                    idoctor.Category = icategory;
                    idoctor.Users = iusers;
                    return idoctor;
                }, new { from = ((current - 1) * size), to = ((current - 1) * size) + size}, splitOn: "Id");

                return Json(new { doctors }, JsonRequestBehavior.AllowGet);
            }
        }

        [Route("page/{current}/{size}/{categoryid}")]
        [HttpGet]
        [EnableCors("MyPolicy")]
        public JsonResult GetByPage(int current, int size, string categoryid)
        {
            using (var db = My.ConnectionFactory())
            {
                string sql = "SELECT  D.*, C.categoryid AS Id, C.* "
                    + " FROM(SELECT    ROW_NUMBER() OVER(ORDER BY doctorname) AS RowNum, * "
                    + "           FROM      Doctor "
                    + "           WHERE     recordstatus <> 2 ";
                if (categoryid != "All")
                {
                    sql += " AND categoryid = @categoryid";
                }
                sql += "         ) AS D "
                    + " LEFT JOIN Category AS C "
                    + " ON D.categoryid = C.categoryid "
                    + " WHERE RowNum >= @from "
                    + "     AND RowNum< @to "
                    + " ORDER BY RowNum";

                IEnumerable<Doctor> doctors = db.Query<Doctor, Category, Doctor>(sql, (idoctor, icategory) =>
                {
                    idoctor.Category = icategory;
                    return idoctor;
                }, new { from = ((current - 1) * size), to = ((current - 1) * size) + size, categoryid = categoryid }, splitOn: "Id");

                return Json(new { doctors }, JsonRequestBehavior.AllowGet);
            }
        }

        [Route("page/{current}/{size}/{categoryid}/{doctorname}")]
        [HttpGet]
        [EnableCors("MyPolicy")]
        public JsonResult GetByPage(int current, int size, string categoryid, string doctorname)
        {
            using (var db = My.ConnectionFactory())
            {
                string sql = "SELECT  D.*, C.categoryid AS Id, C.* "
                    + " FROM(SELECT    ROW_NUMBER() OVER(ORDER BY doctorname) AS RowNum, * "
                    + "           FROM      Doctor "
                    + "           WHERE     recordstatus <> 2 ";
                if (!string.IsNullOrEmpty(doctorname))
                {
                    sql += " AND doctorname LIKE @doctorname";
                }
                if (categoryid != "All")
                {
                    sql += " AND categoryid = @categoryid";
                }
                sql += "         ) AS D "
                    + " LEFT JOIN Category AS C "
                    + " ON D.categoryid = C.categoryid "
                    + " WHERE RowNum >= @from "
                    + "     AND RowNum< @to "
                    + " ORDER BY RowNum";

                IEnumerable<Doctor> doctors = db.Query<Doctor, Category, Doctor>(sql, (idoctor, icategory) =>
                {
                    idoctor.Category = icategory;
                    return idoctor;
                }, new { from = ((current - 1) * size), to = ((current - 1) * size) + size, doctorname = "%" + doctorname + "%", categoryid = categoryid }, splitOn: "Id");

                return Json(new { doctors }, JsonRequestBehavior.AllowGet);
            }
        }

        [Route("bydoctorid/{doctorid}")]
        [HttpGet()]
        [EnableCors("MyPolicy")]
        public JsonResult Get(string doctorid)
        {
            using (var db = My.ConnectionFactory())
            {
                string sql = "SELECT D.*, C.categoryid AS Id, C.* FROM Doctor AS D "
                    + " LEFT JOIN Category AS C "
                    + " ON D.categoryid = C.categoryid"
                    + " WHERE D.recordstatus<> 2 AND D.doctorid = @doctorid";

                Doctor doctor = db.Query<Doctor, Category, Doctor>(sql, (idoctor, icategory) =>
                {
                    idoctor.Category = icategory;
                    return idoctor;
                }, new { doctorid = doctorid }, splitOn: "Id").FirstOrDefault();

                string path = doctor.doctorsource;
                if (!string.IsNullOrEmpty(path) && System.IO.File.Exists(path))
                {
                    byte[] imageByteData = System.IO.File.ReadAllBytes(path);
                    string imageBase64Data = Convert.ToBase64String(imageByteData);
                    string imageDataURL = string.Format("data:image/png;base64,{0}", imageBase64Data);
                    doctor.imagedata = imageDataURL;
                }

                return Json(new { doctor }, JsonRequestBehavior.AllowGet);

                //return Json(new { doctor = db.QuerySingle<Doctor>(My.Table_Doctor.SelectSingle, new { doctorId = doctorid }) }, JsonRequestBehavior.AllowGet);
            }
        }

        [Route("bycategoryid/{categoryid}")]
        [HttpGet()]
        [EnableCors("MyPolicy")]
        public JsonResult GetByCategory(string categoryid)
        {
            using (var db = My.ConnectionFactory())
            {
                string sql = "SELECT D.*, C.categoryid AS Id, C.* FROM Doctor AS D "
                    + " LEFT JOIN Category AS C "
                    + " ON D.categoryid = C.categoryid"
                    + " WHERE D.recordstatus<> 2 AND D.categoryid = @categoryid";

                IEnumerable<Doctor> doctors = db.Query<Doctor, Category, Doctor>(sql, (idoctor, icategory) =>
                {
                    idoctor.Category = icategory;
                    return idoctor;
                }, new { categoryid = categoryid }, splitOn: "Id").ToList();

                foreach (var doctor in doctors)
                {
                    string path = doctor.doctorsource;
                    if (!string.IsNullOrEmpty(path) && System.IO.File.Exists(path))
                    {
                        byte[] imageByteData = System.IO.File.ReadAllBytes(path);
                        string imageBase64Data = Convert.ToBase64String(imageByteData);
                        string imageDataURL = string.Format("data:image/png;base64,{0}", imageBase64Data);
                        doctor.imagedata = imageDataURL;
                    }
                }

                return Json(new { doctors }, JsonRequestBehavior.AllowGet);

                //return Json(new { doctor = db.QuerySingle<Doctor>(My.Table_Doctor.SelectSingle, new { doctorId = doctorid }) }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [EnableCors("MyPolicy")]
        public ActionResult Save(Doctor doctor, HttpPostedFileBase file)
        {
            try
            {
                using (var db = My.ConnectionFactory())
                {
                    if (doctor.doctorid == null)
                    {
                        doctor.doctorid = Guid.NewGuid();
                    }
                    else
                    {
                        //Delete image from server
                        if (System.IO.File.Exists(doctor.doctorsource))
                        {
                            System.IO.File.Delete(doctor.doctorsource);
                        }
                    }

                    var path = string.Empty;
                    if (file != null && file.ContentLength > 0)
                    {
                        var filename = Path.GetFileName(file.FileName);
                        if (!Directory.Exists(Server.MapPath("~/App_Data/UploadAdsFiles")))
                        {
                            Directory.CreateDirectory(Server.MapPath("~/App_Data/UploadAdsFiles"));
                        }
                        path = Path.Combine(Server.MapPath("~/App_Data/UploadAdsFiles"), filename);
                        file.SaveAs(path);
                    }

                    doctor.createddate = (doctor.createddate == null || doctor.createddate.Equals("")) ? AppUtilities.DateToString() : doctor.createddate;
                    doctor.modifieddate = AppUtilities.DateToString();
                    doctor.doctorsource = path;
                    doctor.userid = "thz";
                    doctor.recordstatus = 1;
                    int result = db.Execute($@"IF EXISTS({My.Table_Doctor.SelectSingle}) {My.Table_Doctor.Update} ELSE {My.Table_Doctor.Insert}", doctor);
                }
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Route("doctordelete/{doctorid}")]
        [HttpPost()]
        [EnableCors("MyPolicy")]
        public ActionResult Delete(string doctorid)
        {
            try
            {
                try
                {
                    using (var db = My.ConnectionFactory())
                    {
                        string sql = "Update Doctor Set recordstatus = 2 Where  doctorid = @doctorid";
                        int result = db.Execute(sql, new { doctorid = doctorid });
                    }
                    return Ok();
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Route("doctorid/{doctorid}")]
        [HttpGet]
        public JsonResult GetByDoctorId(string doctorid)
        {
            using (var db = My.ConnectionFactory())
            {
                string sql = "Select D.*, U.userid AS Id, U.* From Doctor AS D " +
                    "LEFT JOIN Users AS U " +
                    "ON D.useraccountid = U.userid "+
                    " WHERE D.recordstatus<> 2 AND D.doctorid = @doctorid";
                //Doctor doctor = db.Query<Doctor,Users,Doctor>(sql, new { doctorid = doctorid }).SingleOrDefault();

                //string sql = "SELECT D.*, C.categoryid AS Id, C.* FROM Doctor AS D "
                //   + " LEFT JOIN Category AS C "
                //   + " ON D.categoryid = C.categoryid"
                //   + " WHERE D.recordstatus<> 2 AND D.categoryid = @categoryid";


                Doctor doctor = db.Query<Doctor, Users, Doctor>(sql, (idoctor, iusers) =>
                {
                    idoctor.Users = iusers;
                    return idoctor;
                }, new { doctorid = doctorid }, splitOn: "Id").FirstOrDefault();

                string path = doctor.doctorsource;
                string imageDataURL = string.Empty;
                if (!string.IsNullOrEmpty(path))
                {
                    byte[] imageByteData = System.IO.File.ReadAllBytes(path);
                    string imageBase64Data = Convert.ToBase64String(imageByteData);
                    imageDataURL = string.Format("data:image/png;base64,{0}", imageBase64Data);
                    //imageDataURL = string.Format("data:image/png;base64,{0},name:{1};", imageBase64Data, Path.GetFileName(path));
                    //doctor.doctorsource = imageDataURL;
                }
                return Json(new { doctor, doctorimage = imageDataURL }, JsonRequestBehavior.AllowGet);
            }
        }

        [Route("approveDoctor/{userid}")]
        [HttpPost()]
        [EnableCors("MyPolicy")]
        public ActionResult approveDoctor(string userid)
        {
            try
            {
            using (var db = My.ConnectionFactory())
                {
                    string sql = "Update Users Set isdoctor = 1 Where userid = @userid";
                    int result = db.Execute(sql, new { userid = userid });
                }
                return Ok();
               
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        private ActionResult BadRequest(string message)
        {
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest, message);
        }

        private ActionResult NotFound()
        {
            return new HttpStatusCodeResult(HttpStatusCode.NotFound);
        }

        private ActionResult Ok()
        {
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
    }
}
