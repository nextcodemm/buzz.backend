﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using Microsoft.AspNetCore.Cors;
//using Dapper;
//using System.Web.Mvc;
//using buzz.backend.Utilities;
//using buzz.backend.Models;
//using System.Net;
//using System.Web;
//using System.IO;

//namespace buzz.backend.Controllers
//{
//    //[ServiceFilter(typeof(AuditFilter))]
//    //[Produces("application/json")]
//    [Route("api/patient")]
//    public class PatientController : Controller
//    {
//        [Authorize]
//        //[HttpGet("{healthcareunitid}")]
//        [Route("api/patient/{healthcareunitid}")]
//        [HttpGet]
//        [EnableCors("MyPolicy")]
//        public IEnumerable<Patient> Get(string healthcareunitid)
//        {
//            string sql = @"SELECT * FROM dbo.Patient AS P
//                           LEFT OUTER JOIN dbo.Counter AS C ON P.counternumber = C.counternumber";

//            using (var db = My.ConnectionFactory())
//            {
//                IEnumerable<Patient> patientList = db.Query<Patient, Counter, Patient>(sql, (patient, counter) =>
//                {
//                    patient.counter = counter;
//                    return patient;

//                }, splitOn: "patientid, counternumber").ToList();
//                return patientList;
//            }
//        }

//        //[HttpGet("pool/{start}/{end}")]
//        [Route("api/patient/pool/{start}/{end}")]
//        [HttpGet]
//        [EnableCors("MyPolicy")]
//        public JsonResult GetPatientPool(int start, int end)
//        {
//            using (var db = My.ConnectionFactory())
//            {
//                string sql = @"SELECT * FROM 
//                               (SELECT ROW_NUMBER() OVER ( ORDER BY P.patientid DESC) AS rownumber,P.*,
//                               (CASE P.admissionstatus WHEN 1 THEN 'InPatient' WHEN 2 THEN 'Discharged' WHEN 4 THEN 'Dead' ELSE 'OPD' END) AS patientstatus
//                               FROM HealthCareUnitPatient AS P) AS result
//                               WHERE rownumber BETWEEN @start AND @end
//                               ORDER BY rownumber";

//                string count = @"SELECT COUNT(P.patientunitid) FROM HealthCareUnitPatient AS P";

//                IEnumerable<PatientListModel> patientList = db.Query<PatientListModel>(sql, new { start = start, end = end }).ToList();
//                int totalCount = db.ExecuteScalar<int>(count);

//                foreach (var item in patientList)
//                {
//                    string admissiondate = item.admissiondate;
//                    if (admissiondate != null) item.admissiondate = AppUtilities.StringToDate(admissiondate);
//                }
//                return Json(new { patientList, totalCount });
//            }
//        }


//        [HttpGet("pool/{start}/{end}/{patientname}")]
//        [EnableCors("MyPolicy")]
//        public JsonResult GetPatientPool(int start, int end, string patientname)
//        {
//            using (var db = My.ConnectionFactory())
//            {
//                string sql = @"SELECT * FROM 
//                               (SELECT ROW_NUMBER() OVER ( ORDER BY P.patientid DESC) AS rownumber,P.*,
//                               (CASE P.admissionstatus WHEN 1 THEN 'InPatient' WHEN 2 THEN 'Discharged' WHEN 4 THEN 'Dead' ELSE 'OPD' END) AS patientstatus
//                               FROM HealthCareUnitPatient AS P
//                               WHERE P.patientname LIKE @patientname) AS result
//                               WHERE rownumber BETWEEN @start AND @end
//                               ORDER BY rownumber";

//                string count = @"SELECT COUNT(P.patientunitid) FROM HealthCareUnitPatient AS P WHERE P.patientname LIKE @patientname";

//                patientname = "%" + patientname + "%";
//                IEnumerable<PatientPoolListModel> patientList = db.Query<PatientPoolListModel>(sql, new { start = start, end = end, patientname = patientname }).ToList();
//                int totalCount = db.ExecuteScalar<int>(count, new { patientname = patientname });

//                foreach (var item in patientList)
//                {
//                    string admissiondate = item.admissiondate;
//                    if (admissiondate != null) item.admissiondate = AppUtilities.StringToDate(admissiondate);
//                }

//                return Json(new { patientList, totalCount });
//            }
//        }

//        [HttpGet("poolcount/{patientname}")]
//        [EnableCors("MyPolicy")]
//        public JsonResult GetPatientPoolCount(string patientname)
//        {
//            using (var db = My.ConnectionFactory())
//            {
//                string count = @"SELECT COUNT(P.patientunitid) FROM HealthCareUnitPatient AS P WHERE P.patientname LIKE @patientname";

//                patientname = "%" + patientname + "%";
//                int totalCount = db.ExecuteScalar<int>(count, new { patientname = patientname });
//                return Json(new { totalCount });
//            }
//        }

//        [HttpGet("poolcount")]
//        [EnableCors("MyPolicy")]
//        public JsonResult GetPatientPoolCount()
//        {
//            using (var db = My.ConnectionFactory())
//            {
//                string count = @"SELECT COUNT(P.patientunitid) FROM HealthCareUnitPatient AS P";
//                int totalCount = db.ExecuteScalar<int>(count);
//                return Json(new { totalCount });
//            }
//        }

//        [Authorize]
//        [HttpGet("{start}/{end}/{healthcareunitid}")]
//        [EnableCors("MyPolicy")]
//        public JsonResult GetPatientList(int start, int end, string healthcareunitid)
//        {
//            using (var db = AppUtilities.GetDbConnection(healthcareunitid))
//            {
//                string sql = @"SELECT * FROM 
//                               (SELECT ROW_NUMBER() OVER ( ORDER BY P.patientid DESC) AS rownumber,P.patientid,P.patientname,P.age, P.gender,P.[address],A.admissionid, A.admissiondate,A.consultantid, D.doctorname, R.[description],
//                               (CASE A.admissionstatus WHEN 1 THEN 'InPatient' WHEN 2 THEN 'Discharged' WHEN 4 THEN 'Dead' ELSE 'OPD' END) AS patientstatus
//                               FROM Patient AS P LEFT JOIN Admission AS A ON P.patientid = A.patientid
//                               LEFT JOIN Doctor AS D ON A.consultantid=D.doctorid
//                               LEFT JOIN Room AS R ON A.roomnumber = R.roomnumber) AS result
//                               WHERE rownumber BETWEEN @start AND @end
//                               ORDER BY rownumber";

//                string count = @"SELECT COUNT(P.patientid) FROM Patient AS P LEFT JOIN Admission AS A ON P.patientid = A.patientid
//                                 LEFT JOIN Doctor AS D ON A.consultantid = D.doctorid
//                                 LEFT JOIN Room AS R ON A.roomnumber = R.roomnumber";
//                IEnumerable<PatientListModel> patientList = db.Query<PatientListModel>(sql, new { start = start, end = end }).ToList();
//                int totalCount = db.ExecuteScalar<int>(count);

//                foreach (var item in patientList)
//                {
//                    string admissiondate = item.admissiondate;
//                    if (admissiondate != null) item.admissiondate = AppUtilities.StringToDate(admissiondate);
//                }

//                return Json(new { patientList, totalCount });
//            }
//        }

//        [Authorize]
//        [HttpGet("{start}/{end}/{patientname}/{healthcareunitid}")]
//        [EnableCors("MyPolicy")]
//        public JsonResult GetPatientListByName(int start, int end, string patientname, string healthcareunitid)
//        {
//            using (var db = AppUtilities.GetDbConnection(healthcareunitid))
//            {
//                string sql = @"SELECT * FROM 
//                               (SELECT ROW_NUMBER() OVER ( ORDER BY P.patientid DESC) AS rownumber,P.patientid,P.patientname,P.age, P.gender,P.[address],A.admissionid, A.admissiondate,A.consultantid, D.doctorname, R.[description],
//                               (CASE A.admissionstatus WHEN 1 THEN 'InPatient' WHEN 2 THEN 'Discharged' WHEN 4 THEN 'Dead' ELSE 'OPD' END) AS patientstatus
//                               FROM Patient AS P LEFT JOIN Admission AS A ON P.patientid = A.patientid
//                               LEFT JOIN Doctor AS D ON A.consultantid=D.doctorid
//                               LEFT JOIN Room AS R ON A.roomnumber = R.roomnumber
//                               WHERE P.patientname LIKE @patientname) AS result
//                               WHERE (rownumber BETWEEN @start AND @end)
//                               ORDER BY rownumber";

//                string count = @"SELECT COUNT(P.patientid) FROM Patient AS P LEFT JOIN Admission AS A ON P.patientid = A.patientid
//                                 LEFT JOIN Doctor AS D ON A.consultantid = D.doctorid
//                                 LEFT JOIN Room AS R ON A.roomnumber = R.roomnumber";

//                patientname = "%" + patientname + "%";
//                IEnumerable<PatientListModel> patientList = db.Query<PatientListModel>(sql, new { start = start, end = end, patientname = patientname }).ToList();
//                int totalCount = db.ExecuteScalar<int>(count);

//                foreach (var item in patientList)
//                {
//                    string admissiondate = item.admissiondate;
//                    if (admissiondate != null) item.admissiondate = AppUtilities.StringToDate(admissiondate);
//                }

//                return Json(new { patientList, totalCount });
//            }
//        }

//        /*[Authorize]
//        [HttpGet("top,{healthcareunitid}")]
//        [EnableCors("MyPolicy")]
//        public IEnumerable<Patient> GetTop(string healthcareunitid)
//        {
//            string sql = @"SELECT TOP 300 * FROM dbo.Patient AS P
//                           LEFT OUTER JOIN dbo.Counter AS C ON P.counternumber = C.counternumber";

//            using (var db = AppUtilities.GetDbConnection(healthcareunitid))
//            {
//                IEnumerable<Patient> patientList = db.Query<Patient, Counter, Patient>(sql, (patient, counter) =>
//                {
//                    patient.counter = counter;
//                    return patient;

//                }, splitOn: "patientid, counternumber").ToList();
//                return patientList;
//            }
//        }*/

//        /*[Authorize]
//        [HttpGet("{id}/{counter}/{healthcareunitid}")]
//        [EnableCors("MyPolicy")]
//        public Patient Get(long id, int counter, string healthcareunitid)
//        {
//            using (var db = AppUtilities.GetDbConnection(healthcareunitid))
//            {
//                Patient patient = db.QuerySingle<Patient>(My.Table_Patient.SelectSingle, new { patientId = id, counterNumber = counter });
//                return patient;
//            }
//        }*/

//        /*[Authorize]
//        [HttpGet("getmax/{counter}/{healthcareunitid}")]
//        [EnableCors("MyPolicy")]
//        public long GetPatientId(int counter, string healthcareunitid)
//        {
//            using (var db = AppUtilities.GetDbConnection(healthcareunitid))
//            {
//                string sql = My.Table_Patient.SelectMaxIdWithComposit;
//                long id = db.Query<int>(sql, new { counterNumber = counter }).Max();
//                return id;
//            }
//        }*/

//        [Authorize]
//        [HttpPost("{healthcareunitid}")]
//        [EnableCors("MyPolicy")]
//        public IActionResult Post([FromBody]Patient value, string healthcareunitid)
//        {
//            try
//            {
//                using (var db = AppUtilities.GetDbConnection(healthcareunitid))
//                {
//                    int result = db.Execute($@"IF EXISTS({My.Table_Patient.SelectSingle}) {My.Table_Patient.Update} ELSE {My.Table_Patient.Insert}", value);
//                }
//                return Ok();
//            }
//            catch (Exception ex)
//            {
//                return BadRequest(ex.Message);
//            }
//        }

//        [Authorize]
//        [HttpDelete("{id}/{counter}/{healthcareunitid}")]
//        [EnableCors("MyPolicy")]
//        public IActionResult Delete(long id, int counter, string healthcareunitid)
//        {
//            try
//            {
//                using (var db = AppUtilities.GetDbConnection(healthcareunitid))
//                {
//                    int result = db.Execute(My.Table_Patient.Delete, new { patientId = id, counterNumber = counter });
//                    if (result > 0) return Ok(); else return NotFound();
//                }
//            }
//            catch (Exception ex)
//            {
//                return BadRequest(ex.Message);
//            }
//        }
//    }
//}
