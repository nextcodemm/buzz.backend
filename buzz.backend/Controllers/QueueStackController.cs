﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Cors;
using Dapper;
using System.Web.Mvc;
using buzz.backend.Utilities;
using buzz.backend.Models;
using System.Net;
using System.Data.Common;
using Newtonsoft.Json.Linq;
using System.Web.Helpers;
using Newtonsoft.Json;

namespace buzz.backend.Controllers
{
    //[Produces("application/json")]
    [Route("api/queuestacks")]
    public class QueueStackController : Controller
    {
        [HttpGet]
        [EnableCors("MyPolicy")]
        public JsonResult Get()
        {
            using (var db = My.ConnectionFactory())
            {
                return Json(new { queuestacks = db.Query<QueueStack>(My.Table_QueueStack.Select).ToList() }, JsonRequestBehavior.AllowGet);
            }
        }

        [Route("api/queuestacks/{id}")]
        [HttpGet]
        [EnableCors("MyPolicy")]
        public JsonResult Get(string id)
        {
            using (var db = My.ConnectionFactory())
            {
                var queuestack = db.QuerySingle<QueueStack>(My.Table_QueueStack.SelectSingle, new { queuestackid = id });
                return Json(new { queuestack = queuestack }, JsonRequestBehavior.AllowGet);
            }
        }

        [Route("api/queuestacks/next/{countercategoryid}/{counterid}")]
        [HttpGet]
        [EnableCors("MyPolicy")]
        public JsonResult Next(string countercategoryid, Guid? counterid)
        {
            using (var db = My.ConnectionFactory())
            {
                string sqlQuery = "SELECT TOP(1) * FROM  QueueStack "
                    + " WHERE queuedate = CONVERT(VARCHAR(8), GETDATE(), 112) AND countercategoryid = @countercategoryid AND counterid IS NULL "
                    + " ORDER BY queuenumber";

                var queuestack = db.QuerySingleOrDefault<QueueStack>(sqlQuery, new { countercategoryid = countercategoryid });
                
                if( queuestack != null)
                {
                    queuestack.counterid = counterid;
                    queuestack.queuestatus = "serve";
                    queuestack.servetime = DateTime.Now;

                    Save(queuestack);
                }

                return Json(new { queuestack = queuestack }, JsonRequestBehavior.AllowGet);
            }
        }

        [Route("api/queuestacks/reserve/{countercategoryid}/{counterid}")]
        [HttpGet]
        [EnableCors("MyPolicy")]
        public JsonResult Reserve(string countercategoryid, Guid? counterid)
        {
            using (var db = My.ConnectionFactory())
            {
                string sqlQuery = "SELECT TOP(1) * FROM  QueueStack "
                    + " WHERE queuedate = CONVERT(VARCHAR(8), GETDATE(), 112) AND countercategoryid = @countercategoryid AND counterid IS NULL "
                    + " ORDER BY queuenumber";

                var queuestack = db.QuerySingleOrDefault<QueueStack>(sqlQuery, new { countercategoryid = countercategoryid });

                if (queuestack != null)
                {
                    queuestack.counterid = counterid;
                    queuestack.queuestatus = "reserve";
                    queuestack.reservetime = DateTime.Now;

                    Save(queuestack);
                }

                return Json(new { queuestack = queuestack }, JsonRequestBehavior.AllowGet);
            }
        }

        [Route("api/queuestacks/serve/{queuestackid}")]
        [HttpGet]
        [EnableCors("MyPolicy")]
        public JsonResult Serve(Guid? queuestackid)
        {
            using (var db = My.ConnectionFactory())
            {
                var queuestack = db.QuerySingleOrDefault<QueueStack>(My.Table_QueueStack.SelectSingle, new { queuestackid = queuestackid });

                if (queuestack != null)
                {
                    queuestack.queuestatus = "serve";
                    queuestack.servetime = DateTime.Now;
                    Save(queuestack);
                }

                return Json(new { queuestack = queuestack }, JsonRequestBehavior.AllowGet);
            }
        }

        [Route("api/queuestacks/end/{queuestackid}")]
        [HttpGet]
        [EnableCors("MyPolicy")]
        public JsonResult End(Guid? queuestackid)
        {
            using (var db = My.ConnectionFactory())
            {
                var queuestack = db.QuerySingleOrDefault<QueueStack>(My.Table_QueueStack.SelectSingle, new { queuestackid = queuestackid });

                if (queuestack != null)
                {
                    queuestack.queuestatus = "finish";
                    queuestack.endtime = DateTime.Now;

                    Save(queuestack);
                }

                return Json(new { queuestack = queuestack }, JsonRequestBehavior.AllowGet);
            }
        }

        [Route("api/queuestacks/cancel/{queuestackid}")]
        [HttpGet]
        [EnableCors("MyPolicy")]
        public JsonResult Cancel(Guid? queuestackid)
        {
            using (var db = My.ConnectionFactory())
            {
                var queuestack = db.QuerySingleOrDefault<QueueStack>(My.Table_QueueStack.SelectSingle, new { queuestackid = queuestackid });

                if (queuestack != null)
                {
                    queuestack.queuestatus = "cancel";

                    Save(queuestack);
                }

                return Json(new { queuestack = queuestack }, JsonRequestBehavior.AllowGet);
            }
        }

        [Route("api/queuestacks/miss/{queuestackid}")]
        [HttpGet]
        [EnableCors("MyPolicy")]
        public JsonResult Miss(Guid? queuestackid)
        {
            using (var db = My.ConnectionFactory())
            {
                var queuestack = db.QuerySingleOrDefault<QueueStack>(My.Table_QueueStack.SelectSingle, new { queuestackid = queuestackid });

                if (queuestack != null)
                {
                    queuestack.queuestatus = "miss";

                    Save(queuestack);
                }

                return Json(new { queuestack = queuestack }, JsonRequestBehavior.AllowGet);
            }
        }

        [Route("api/queuestacks/currentqueue/{counterid}")]
        [HttpGet]
        [EnableCors("MyPolicy")]
        public JsonResult CurrentQueue(Guid? counterid)
        {
            using (var db = My.ConnectionFactory())
            {
                string sqlQuery = "SELECT TOP(1) * FROM QueueStack AS Q "
                    + " WHERE queuedate = CONVERT(VARCHAR(8), GETDATE(), 112) AND counterid = @counterid AND queuestatus = 'serve' "
                    + " ORDER BY queuenumber DESC";

                var queuestack = db.QuerySingleOrDefault<QueueStack>(sqlQuery, new { counterid = counterid });

                return Json(new { queuestack = queuestack }, JsonRequestBehavior.AllowGet);
            }
        }

        [Route("api/queuestacks/currentreservequeue/{counterid}")]
        [HttpGet]
        [EnableCors("MyPolicy")]
        public JsonResult CurrentReserveQueue(Guid? counterid)
        {
            using (var db = My.ConnectionFactory())
            {
                string sqlQuery = "SELECT TOP(1) * FROM QueueStack AS Q "
                    + " WHERE queuedate = CONVERT(VARCHAR(8), GETDATE(), 112) AND counterid = @counterid AND queuestatus = 'reserve' "
                    + " ORDER BY queuenumber ASC";

                var queuestack = db.QuerySingleOrDefault<QueueStack>(sqlQuery, new { counterid = counterid });

                return Json(new { queuestack = queuestack }, JsonRequestBehavior.AllowGet);
            }
        }

        [Route("api/queuestacks/previousqueuelist/{counterid}")]
        [HttpGet]
        [EnableCors("MyPolicy")]
        public JsonResult PreviousQueueList(Guid? counterid)
        {
            using (var db = My.ConnectionFactory())
            {
                string sqlQuery = "SELECT TOP(4) * FROM QueueStack AS Q "
                    + " WHERE queuedate = CONVERT(VARCHAR(8), GETDATE(), 112) AND counterid = @counterid AND queuestatus <> 'serve' AND queuestatus <> 'reserve' "
                    + " ORDER BY queuenumber DESC";

                IEnumerable<QueueStack> queuestacklist = db.Query<QueueStack>(sqlQuery, new { counterid = counterid }).ToList();

                return Json(new { queuestacklist = queuestacklist }, JsonRequestBehavior.AllowGet);
            }
        }

        [Route("api/queuestacks/upcomingqueuelist/{countercategoryid}")]
        [HttpGet]
        [EnableCors("MyPolicy")]
        public JsonResult UpcomingQueueList(int countercategoryid)
        {
            using (var db = My.ConnectionFactory())
            {
                string sqlQuery = "SELECT * FROM QueueStack AS Q  "
                    + " WHERE queuedate = CONVERT(VARCHAR(8), GETDATE(), 112) "
                    + " AND countercategoryid = @countercategoryid"
                    + " AND queuestatus = 'wait' "
                    + " ORDER BY ISNULL(queueagainorder,9999), queuenumber ASC";

                IEnumerable<QueueStack> queuestacklist = db.Query<QueueStack>(sqlQuery, new { countercategoryid = countercategoryid }).ToList();

                return Json(new { queuestacklist = queuestacklist }, JsonRequestBehavior.AllowGet);
            }
        }

        [Route("api/queuestacks/upcomingqueuelistforqueueagain/{countercategoryid}")]
        [HttpGet]
        [EnableCors("MyPolicy")]
        public JsonResult UpcomingQueueListForQueueAgain(int countercategoryid)
        {
            using (var db = My.ConnectionFactory())
            {
                string sqlQuery = "SELECT * FROM QueueStack AS Q  "
                    + " WHERE queuedate = CONVERT(VARCHAR(8), GETDATE(), 112) "
                    + " AND countercategoryid = @countercategoryid"
                    + " AND queuestatus = 'wait' "
                    + " AND queueagainorder IS NULL"
                    + " ORDER BY queuenumber ASC";

                IEnumerable<QueueStack> queuestacklist = db.Query<QueueStack>(sqlQuery, new { countercategoryid = countercategoryid }).ToList();

                return Json(new { queuestacklist = queuestacklist }, JsonRequestBehavior.AllowGet);
            }
        }

        [Route("api/queuestacks/missqueuelist/{countercategoryid}")]
        [HttpGet]
        [EnableCors("MyPolicy")]
        public JsonResult MissQueueList(int countercategoryid)
        {
            using (var db = My.ConnectionFactory())
            {
                string sqlQuery = "SELECT * FROM QueueStack AS Q  "
                    + " WHERE queuedate = CONVERT(VARCHAR(8), GETDATE(), 112) "
                    + " AND countercategoryid = @countercategoryid"
                    + " AND queuestatus = 'miss' "
                    + " ORDER BY queuenumber ASC";

                IEnumerable<QueueStack> queuestacklist = db.Query<QueueStack>(sqlQuery, new { countercategoryid = countercategoryid }).ToList();

                return Json(new { queuestacklist = queuestacklist }, JsonRequestBehavior.AllowGet);
            }
        }

        [Route("api/queuestacks/queueagain/{queuestackid}")]
        [HttpGet]
        [EnableCors("MyPolicy")]
        public JsonResult QueueAgain(Guid? queuestackid)
        {
            using (var db = My.ConnectionFactory())
            {
                var queuestack = db.QuerySingleOrDefault<QueueStack>(My.Table_QueueStack.SelectSingle, new { queuestackid = queuestackid });
                if (queuestack != null)
                {
                    var maxqueueagainorder = MaxQueueAgainOrder(queuestack.countercategoryid);

                    string UpcomingJson = JsonConvert.SerializeObject( UpcomingQueueListForQueueAgain(queuestack.countercategoryid).Data);
                    JObject o = JObject.Parse(UpcomingJson);
                    List<QueueStack> queuestacklist = o["queuestacklist"].ToObject<List<QueueStack>>();
                    for (int index = 0; index < 3; index++)
                    {
                        if(queuestacklist.Count > index)
                        {
                            queuestacklist[index].queueagainorder = maxqueueagainorder++;
                            Save(queuestacklist[index]);
                        }
                    }

                    queuestack.counterid = null;
                    queuestack.queueagainorder = maxqueueagainorder++;
                    queuestack.queuestatus = "wait";
                     
                    Save(queuestack);
                }

                return Json(new { queuestack = queuestack }, JsonRequestBehavior.AllowGet);
            }
        }

        [Route("api/queuestacks/maxqueueagainorder/{countercategoryid}")]
        [HttpGet]
        [EnableCors("MyPolicy")]
        public int MaxQueueAgainOrder(int countercategoryid)
        {
            using (var db = My.ConnectionFactory())
            {
                string sqlQuery = "SELECT ISNULL(Max(queueagainorder),0) FROM QueueStack AS Q "
                    + " WHERE queuedate = CONVERT(VARCHAR(8), GETDATE(), 112) "
                    + " AND countercategoryid = @countercategoryid "
                    + " AND queuestatus = 'wait' ";

                var maxqueueagainorder = db.ExecuteScalar<int>(sqlQuery, new { countercategoryid = countercategoryid });
                return maxqueueagainorder + 1;
            }
        }

        [HttpPost]
        [EnableCors("MyPolicy")]
        public ActionResult Save(QueueStack queuestack)
        {
            try
            {
                using (var db = My.ConnectionFactory())
                {
                    if (queuestack.queuestackid == null)
                    {
                        queuestack.queuestackid = Guid.NewGuid();
                    }
                    queuestack.createddate = (queuestack.createddate == null || queuestack.createddate.Equals("")) ? AppUtilities.DateToString() : queuestack.createddate;
                    queuestack.modifieddate = AppUtilities.DateToString();
                    queuestack.userid = "thz";
                    queuestack.recordstatus = 1;
                    int result = db.Execute($@"IF EXISTS({My.Table_QueueStack.SelectSingle}) {My.Table_QueueStack.Update} ELSE {My.Table_QueueStack.Insert}", queuestack);
                }
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Route("api/queuestacks/createnew")]
        [HttpPost]
        [EnableCors("MyPolicy")]
        public ActionResult CreateNew(QueueStack queuestack)
        {
            try
            {
                using (var db = My.ConnectionFactory())
                {
                    if (queuestack.queuestackid == null)
                    {
                        queuestack.queuestackid = Guid.NewGuid();
                    }

                    if(queuestack.queuenumber == null)
                    {
                        queuestack.queuenumber = GetMaxQueueNumber(queuestack.queuedate, db);
                    }

                    queuestack.createddate = (queuestack.createddate == null || queuestack.createddate.Equals("")) ? AppUtilities.DateToString() : queuestack.createddate;
                    queuestack.modifieddate = AppUtilities.DateToString();
                    queuestack.userid = "thz";
                    queuestack.recordstatus = 1;
                    int result = db.Execute($@"IF EXISTS({My.Table_QueueStack.SelectSingle}) {My.Table_QueueStack.Update} ELSE {My.Table_QueueStack.Insert}", queuestack);
                }
                return Ok(queuestack.queuenumber);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        private string GetMaxQueueNumber(string queuedatevalue, DbConnection db)
        {
            string sqlQuery = "SELECT RIGHT('0000' + CONVERT(VARCHAR(4), ISNULL(MAX(queuenumber),0) + 1),4) FROM QueueStack AS Q "
                + " WHERE Q.queuedate = @queuedate";
            CommandDefinition cmd =  new CommandDefinition(sqlQuery,new { queuedate = queuedatevalue });
            var result = db.ExecuteScalar(cmd);
            return result.ToString();
        }

        [Route("api/queuestacks/{queuestackid}")]
        [HttpDelete]
        [EnableCors("MyPolicy")]
        public ActionResult Delete(string queuestackid)
        {
            try
            {
                using (var db = My.ConnectionFactory())
                {
                    int result = db.Execute(My.Table_QueueStack.Delete, new { queuestackid = queuestackid });
                    if (result > 0) return Ok(); else return NotFound();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        private ActionResult BadRequest(string message)
        {
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest, message);
        }

        private ActionResult NotFound()
        {
            return new HttpStatusCodeResult(HttpStatusCode.NotFound);
        }

        private ActionResult Ok()
        {
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        private ActionResult Ok(string message)
        {
            return new HttpStatusCodeResult(HttpStatusCode.OK, message);
        }

    }
}
