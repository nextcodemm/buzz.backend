﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Cors;
using Dapper;
using infoboard.backend.Utilities;
using infoboard.backend.Models;
using System.Web.Mvc;
using System.Net;
using System.Web;
using System.IO;
using System.Xml;


namespace infoboard.backend.Controllers
{
    [Route("api/ads")]
    public class AdsSettingController : Controller
    {
        protected override JsonResult Json(object data, string contentType, System.Text.Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new JsonResult()
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior,
                MaxJsonLength = Int32.MaxValue
            };
        }

        [HttpPost]
        [EnableCors("MyPolicy")]
        //public ActionResult Save(AdsSetting adssetting,HttpPostedFileBase file)
        //public ActionResult Save(String adscompany, HttpPostedFileBase file, HttpPostedFileBase[] multifile, Object obj, Ads[] ads)
        //public ActionResult Save(String adscompany, HttpPostedFileBase[] file, Object obj, Ads[] ads)
        //public ActionResult Save(String adscompany, HttpPostedFileBase file, IEnumerable<HttpPostedFileBase> multifile, Object obj, Ads adsinfo)
        //public ActionResult Save(Ads adsinfo)
        public ActionResult Save(IEnumerable<String> adslist, IEnumerable<String> removeadslist, IEnumerable<HttpPostedFileBase> multifile, IEnumerable<String> adssettinglist, IEnumerable<String> removeadssettinglist)
        {
            try
            {
                Guid adsgroupid = Guid.NewGuid();

                using (var db = My.ConnectionFactory())
                {
                    foreach (string adsString in adslist)
                    {
                        Ads Ads = Newtonsoft.Json.JsonConvert.DeserializeObject<Ads>(adsString);
                        if (Ads != null)
                        {
                            if (Ads.adsgroupid == null)
                            {
                                Ads.adsgroupid = adsgroupid;
                            }
                            else
                            {
                                adsgroupid = (Guid)Ads.adsgroupid;
                            }

                            if (Ads.adsid == null)
                            {
                                Ads.adsid = Guid.NewGuid();
                            }

                            string path = string.Empty;
                            foreach (var item in multifile)
                            {
                                if (item.ContentLength > 0 && Ads.adssource != string.Empty)
                                {
                                    var filename = Path.GetFileName(item.FileName);
                                    if (filename.ToUpper() == Ads.adssource.ToUpper())
                                    {
                                        if (!Directory.Exists(Server.MapPath("~/App_Data/UploadAdsFiles")))
                                        {
                                            Directory.CreateDirectory(Server.MapPath("~/App_Data/UploadAdsFiles"));
                                        }
                                        path = Path.Combine(Server.MapPath("~/App_Data/UploadAdsFiles"), filename);
                                        item.SaveAs(path);
                                        Ads.adssource = path;

                                        Ads.createddate = (Ads.createddate == null || Ads.createddate.Equals("")) ? AppUtilities.DateToString() : Ads.createddate;
                                        Ads.modifieddate = AppUtilities.DateToString();
                                        Ads.userid = "thz";
                                        Ads.recordstatus = 1;
                                        int result = db.Execute($@"IF EXISTS({My.Table_Ads.SelectSingle}) {My.Table_Ads.Update} ELSE {My.Table_Ads.Insert}", Ads);

                                        break;
                                    }
                                }
                            }
                        }
                    }

                    if(adssettinglist.Count() > 0)
                    {
                        AdsSettingDeleteByAdsGroup(adsgroupid.ToString());
                    }

                    foreach (String adssettingString in adssettinglist)
                    {
                        AdsSetting adssetting = Newtonsoft.Json.JsonConvert.DeserializeObject<AdsSetting>(adssettingString);

                        //if (adssetting.adssettingid == null)
                        //{
                            adssetting.adssettingid = Guid.NewGuid();
                        //}
                        adssetting.createddate = (adssetting.createddate == null || adssetting.createddate.Equals("")) ? AppUtilities.DateToString() : adssetting.createddate;
                        adssetting.modifieddate = AppUtilities.DateToString();
                        adssetting.adsgroupid = adsgroupid;
                        adssetting.userid = "thz";
                        adssetting.recordstatus = 1;
                        int result = db.Execute($@"IF EXISTS({My.Table_AdsSetting.SelectSingle}) {My.Table_AdsSetting.Update} ELSE {My.Table_AdsSetting.Insert}", adssetting);

                    }

                    if (removeadslist != null)
                    {
                        foreach (string adsString in removeadslist)
                        {
                            Ads Ads = Newtonsoft.Json.JsonConvert.DeserializeObject<Ads>(adsString);
                            if (Ads != null)
                            {
                                AdsDelete(Ads.adsid.ToString());
                            }
                        }
                    }

                    if (removeadssettinglist != null)
                    {
                        foreach (String adssettingString in removeadssettinglist)
                        {
                            AdsSetting adssetting = Newtonsoft.Json.JsonConvert.DeserializeObject<AdsSetting>(adssettingString);
                            if (adssetting != null)
                            {
                                AdsSettingDelete(adssetting.adssettingid.ToString());
                            }
                        }
                    }



                }

                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        [HttpDelete()]
        [EnableCors("MyPolicy")]
        public ActionResult AdsDelete(string id)
        {
            try
            {
                using (var db = My.ConnectionFactory())
                {
                    int result = db.Execute(My.Table_Ads.Delete, new { adsid = id });
                    if (result > 0) return Ok(); else return NotFound();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete()]
        [EnableCors("MyPolicy")]
        public ActionResult AdsSettingDelete(string id)
        {
            try
            {
                using (var db = My.ConnectionFactory())
                {
                    int result = db.Execute(My.Table_AdsSetting.Delete, new { adssettingid = id });
                    if (result > 0) return Ok(); else return NotFound();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete()]
        [EnableCors("MyPolicy")]
        public ActionResult AdsSettingDeleteByAdsGroup(string adsgroupid)
        {
            try
            {
                using (var db = My.ConnectionFactory())
                {
                    int result = db.Execute("DELETE FROM AdsSetting WHERE adsgroupid = @adsgroupid", new { adsgroupid = adsgroupid });
                    if (result > 0) return Ok(); else return NotFound();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Route("api/adsgroup")]
        [HttpGet()]
        [EnableCors("MyPolicy")]
        public JsonResult Get()
        {
            using (var db = My.ConnectionFactory())
            {
                string sqlQuery = "SELECT adsgroupid, adscompany, adsname FROM Ads "
                     + " WHERE recordstatus<> 2 "
                     + " GROUP BY adsgroupid, adscompany, adsname";

                IEnumerable<Ads> adsgrouplist = db.Query<Ads>(sqlQuery).ToList();

                return Json(new { adsgrouplist }, JsonRequestBehavior.AllowGet);
            }
        }

        [Route("api/adsgroup/{adsgroupid}")]
        [HttpGet()]
        [EnableCors("MyPolicy")]
        public JsonResult Get(Guid? adsgroupid)
        {
            using (var db = My.ConnectionFactory())
            {
                string adsQuery = "SELECT * FROM Ads "
                    + " WHERE recordstatus<> 2 AND adsgroupid = @adsgroupid";

                IEnumerable<AdsVM> adslist = db.Query<AdsVM>(adsQuery, new { adsgroupid = adsgroupid }).ToList();

                foreach (AdsVM ads in adslist)
                {
                    string path = ads.adssource;
                    if (!string.IsNullOrEmpty(path) && System.IO.File.Exists(path))
                    {
                        byte[] imageByteData = System.IO.File.ReadAllBytes(path);
                        string imageBase64Data = Convert.ToBase64String(imageByteData);
                        string imageDataURL = string.Format("data:image/png;base64,{0}", imageBase64Data);

                        ads.adsfile = imageDataURL;
                        ads.adssource = Path.GetFileName(path);
                    }
                }

                string adssettingQuery = "SELECT *,D.displayboardid AS Id, D.* FROM AdsSetting AS A "
                    + " LEFT JOIN DisplayBoard AS D "
                    + " ON A.displayboardid = D.displayboardid "
                    + " WHERE A.recordstatus <> 2 AND A.adsgroupid =  @adsgroupid";

                IEnumerable<AdsSetting> adssettinglist = db.Query<AdsSetting, DisplayBoard, AdsSetting>(adssettingQuery, (adsSetting, displayBoard) =>
                {
                    adsSetting.DisplayBoard = displayBoard;
                    return adsSetting;
                }, new { adsgroupid = adsgroupid }, splitOn: "Id").ToList();

                return Json(new { adslist, adssettinglist }, JsonRequestBehavior.AllowGet);
            }
        }

        [Route("api/adsbyadsgroup/{adsgroupid}")]
        [HttpGet()]
        [EnableCors("MyPolicy")]
        public JsonResult GetAdsByAdsGroup(Guid? adsgroupid)
        {
            using (var db = My.ConnectionFactory())
            {
                string adsQuery = "SELECT * FROM Ads "
                    + " WHERE recordstatus<> 2 AND adsgroupid = @adsgroupid";

                IEnumerable<AdsVM> adslist = db.Query<AdsVM>(adsQuery, new { adsgroupid = adsgroupid }).ToList();

                foreach (AdsVM ads in adslist)
                {
                    string path = ads.adssource;
                    if (!string.IsNullOrEmpty(path) && System.IO.File.Exists(path))
                    {
                        byte[] byteData = System.IO.File.ReadAllBytes(path);
                        string base64Data = Convert.ToBase64String(byteData);
                        ads.adsfiletype = Path.GetExtension(path);
                        string imageDataURL = string.Empty;
                        if (ads.adsfiletype == ".mp4" || ads.adsfiletype == ".wemb" || ads.adsfiletype == ".ogg")
                        {
                            //imageDataURL = string.Format("data:video/mp4;base64,{0}", base64Data);
                            //imageDataURL = string.Format("data:video/MPEG-4;base64,{0}", base64Data);
                            imageDataURL = Path.GetFileName(path);
                        }
                        else
                        {
                            imageDataURL = string.Format("data:image/png;base64,{0}", base64Data);
                        }

                        ads.adsfile = imageDataURL;
                        ads.adssource = Path.GetFileName(path);
                    }
                }

                return Json(new { adslist }, JsonRequestBehavior.AllowGet);
            }
        }


        [Route("api/adsgroup/{adsgroupid}")]
        [HttpDelete()]
        [EnableCors("MyPolicy")]
        public ActionResult DeleteAdsGroup(string adsgroupid)
        {
            try
            {
                using (var db = My.ConnectionFactory())
                {
                    int result = db.Execute("DELETE FROM Ads WHERE adsgroupid = @adsgroupid", new { adsgroupid = adsgroupid });
                    result = db.Execute("DELETE FROM AdsSetting WHERE adsgroupid = @adsgroupid", new { adsgroupid = adsgroupid });
                    if (result > 0) return Ok(); else return NotFound();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


        private ActionResult BadRequest(string message)
        {
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest, message);
        }

        private ActionResult NotFound()
        {
            return new HttpStatusCodeResult(HttpStatusCode.NotFound);
        }

        private ActionResult Ok()
        {
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
    }
}