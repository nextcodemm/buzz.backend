﻿using System;
using System.Text;
using System.Security.Claims;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Cors;
using Dapper;
using System.Web.Mvc;
using buzz.backend.Utilities;
using buzz.backend.Models;
using System.Net;
using System.IdentityModel.Tokens;

namespace buzz.backend.Controllers
{
    //[Produces("application/json")]
    [Route("api/account")]
    public class UserAccountController : Controller
    {
        [Route("api/account/token")]
        [HttpPost()]
        public JsonResult RequestToken(UserAccount appUser)
        {
            IEnumerable<UserAccount> accounts = ValidateUser(appUser.username, appUser.password);
            if (accounts == null) return null;
            UserAccount account = null;
            foreach (UserAccount useracc in accounts)
            {
                if (useracc.email.Equals(appUser.username))
                {
                    account = useracc;
                    break;
                }
            }
            string tokenText = "";
            if (account != null)
            {
                account.createddate = AppUtilities.StringToDate(account.createddate);
                var claims = new Claim[] {
                    new Claim(ClaimTypes.Name, appUser.username),
                    new Claim(JwtRegisteredClaimNames.Email, appUser.username),
                    new Claim("roles","admin")
                };

                //var key = new Microsoft.IdentityModel.Tokens.SymmetricSecurityKey(Encoding.UTF8.GetBytes("nextcode@20171001"));
                //var creds = new Microsoft.IdentityModel.Tokens.SigningCredentials(key, Microsoft.IdentityModel.Tokens.SecurityAlgorithms.HmacSha256);
                var key = new InMemorySymmetricSecurityKey(Encoding.UTF8.GetBytes("nextcode@20171001"));
                var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256Signature, SecurityAlgorithms.Sha256Digest);

                var token = new JwtSecurityToken(
                    issuer: "HospiBook",
                    audience: "HospiBookWeb",
                    claims: claims,
                    notBefore: DateTime.Now,
                    expires: DateTime.Now.AddMinutes(30),
                    signingCredentials: creds);

                tokenText = new JwtSecurityTokenHandler().WriteToken(token);

                //var tokenHandler = new JwtSecurityTokenHandler();

                //TokenValidationParameters tvps = new TokenValidationParameters
                //{
                //   ValidateIssuerSigningKey = true,
                //    IssuerSigningKey = new InMemorySymmetricSecurityKey(Encoding.UTF8.GetBytes("nextcode@20171001")),

                //    ValidateIssuer = true,
                //    ValidIssuer = "InfoBoard",

                //    ValidateAudience = true,
                //    ValidAudience = "InfoBoardWeb",

                //    ValidateLifetime = true, //validate the expiration and not before values in the token

                //    ClockSkew = TimeSpan.FromMinutes(5) //5 minute tolerance for the expiration date
                //};

                //SecurityToken validatedToken;
                //tokenHandler.ValidateToken(tokenText,
                //    tvps, out validatedToken);
                //Console.WriteLine(validatedToken.ToString());
                //Console.ReadLine();

                return Json(new { tokenText, account });
            }

            tokenText = "Could not verify username and password";
            return Json(new { tokenText, account });
        }

        [Route("api/useraccount/login")]
        [HttpPost()]
        public JsonResult Login(UserAccount appUser)
        {
            //if (appUser.username == "admin" && appUser.password == "admin")
            //{
            //    //var apistatus = "OK";
            //    //var apitext = "Valid";
            //    //return Json(new { apistatus, apitext });
            //    return JsonReturn(AppUtilities.JsonStatus.OK, "Valid");
            //}
            //else
            //{
            //    //var apistatus = "ERROR";
            //    //var apitext = "Username or password invalid!";
            //    //return Json(new { apistatus, apitext });
            //    return JsonReturn(AppUtilities.JsonStatus.ERROR, "Username or password invalid!");
            //}
            if(TryLogin(appUser.email, appUser.password))
            {
                //var tokenText = GetAdminTokenText(appUser);
                var result = "success";
                return JsonReturn(AppUtilities.JsonStatus.OK, result);
            }
            else
            {
                return JsonReturn(AppUtilities.JsonStatus.ERROR, "Username or password invalid!");
            }

        }

        private string GetAdminTokenText(UserAccount appUser)
        {
            string tokenText = string.Empty;

            var claims = new Claim[] {
                    new Claim(ClaimTypes.Name, appUser.mobile),
                    new Claim(JwtRegisteredClaimNames.Sub, appUser.mobile),
                    new Claim("roles","admin")
                };

            //var key = new Microsoft.IdentityModel.Tokens.SymmetricSecurityKey(Encoding.UTF8.GetBytes("nextcode@20171001"));
            //var creds = new Microsoft.IdentityModel.Tokens.SigningCredentials(key, Microsoft.IdentityModel.Tokens.SecurityAlgorithms.HmacSha256);
            var key = new InMemorySymmetricSecurityKey(Encoding.UTF8.GetBytes("nextcode@20171001"));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256Signature, SecurityAlgorithms.Sha256Digest);

            var token = new JwtSecurityToken(
                issuer: "HospiBook",
                audience: "HospiBookWeb",
                claims: claims,
                notBefore: DateTime.Now,
                expires: DateTime.Now.AddMinutes(30),
                signingCredentials: creds);

            tokenText = new JwtSecurityTokenHandler().WriteToken(token);


            return tokenText;
        }

        private string GetUserTokenText(UserAccount appUser)
        {
            string tokenText = string.Empty;

            var claims = new Claim[] {
                    new Claim(ClaimTypes.Name, appUser.mobile),
                    new Claim(JwtRegisteredClaimNames.Sub, appUser.mobile),
                    new Claim("roles","user")
                };

            //var key = new Microsoft.IdentityModel.Tokens.SymmetricSecurityKey(Encoding.UTF8.GetBytes("nextcode@20171001"));
            //var creds = new Microsoft.IdentityModel.Tokens.SigningCredentials(key, Microsoft.IdentityModel.Tokens.SecurityAlgorithms.HmacSha256);
            var key = new InMemorySymmetricSecurityKey(Encoding.UTF8.GetBytes("nextcode@20171001"));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256Signature, SecurityAlgorithms.Sha256Digest);

            var token = new JwtSecurityToken(
                issuer: "HospiBook",
                audience: "HospiBookWeb",
                claims: claims,
                notBefore: DateTime.Now,
                expires: DateTime.Now.AddMinutes(30),
                signingCredentials: creds);

            tokenText = new JwtSecurityTokenHandler().WriteToken(token);


            return tokenText;
        }

        private string CodeGen(int length)
        {
            Random rdn = new Random();
            int randValue = rdn.Next(Convert.ToInt32(Math.Pow(10, length)));
            string randFormat = "d" + length;

            var gencode = String.Format("{0:" + randFormat + "}", randValue);
            return gencode;
        }

        private IEnumerable<UserAccount> ValidateUser(string username, string password)
        {
            if (TryLogin(username, password))
            {
                string sql = @"SELECT usr.useraccountid, usr.createddate, usr.modifieddate, usr.username, usr.email, usr.accountstatus, usr.userid, usr.recordstatus
                           FROM UserAccount AS usr WHERE email=@email
                           SELECT U.useraccountid, HU.healthcareunitid, H.healthcareunitname, T.unittypeid, H.address, H.phone, H.contactperson
                           FROM UserAccount AS U INNER JOIN HealthCareUnitUser AS HU ON U.useraccountid=HU.useraccountid
                           INNER JOIN HealthCareUnit AS H ON HU.healthcareunitid=H.healthcareunitid 
                           INNER JOIN HealthCareUnitType AS T ON H.unittypeid=T.unittypeid
                           WHERE U.email=@email";

                using (var db = My.ConnectionFactory())
                {
                    var mapper = db.QueryMultiple(sql, new { email = username });
                    var users = mapper.Read<UserAccount>().ToDictionary(k => k.useraccountid, v => v);
                    var healthCareUnitUsers = mapper.Read<HealthCareUnitUser>().ToList();

                    foreach (var healthCareUnitUser in healthCareUnitUsers.GroupBy(g => g.useraccountid))
                    {
                        users[healthCareUnitUser.Key].healthCareUnitUsers = healthCareUnitUser.ToList();
                    }

                    return users.Values.ToList();
                }
            }
            return null;
        }

        private bool TryLogin(string email, string password)
        {
            using (var db = My.ConnectionFactory())
            {
                var sql = "usplogin";
                var _params = new DynamicParameters();
                _params.Add(
                    name: "@email",
                    value: email,
                    dbType: System.Data.DbType.String,
                    direction: System.Data.ParameterDirection.Input
                    );
                _params.Add(
                    name: "@password",
                    value: password,
                    dbType: System.Data.DbType.String,
                    direction: System.Data.ParameterDirection.Input
                    );
                _params.Add(
                    name: "@responseMessage",
                    dbType: System.Data.DbType.Int32,
                    direction: System.Data.ParameterDirection.ReturnValue
                    );

                var affectedRows = db.Execute(sql, _params, commandType: System.Data.CommandType.StoredProcedure);
                int responseMessage = _params.Get<int>("@responseMessage");

                if (responseMessage == 2)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        private bool TryRegister(string username, string password, string mobile, string verifycode, string verifystatus, out Guid?useraccountid)
        {
            using (var db = My.ConnectionFactory())
            {
                var sql = "uspAddUser";
                var _params = new DynamicParameters();
                _params.Add(
                    name: "@pLogin",
                    value: username,
                    dbType: System.Data.DbType.String,
                    direction: System.Data.ParameterDirection.Input
                    );
                _params.Add(
                    name: "@pMobile",
                    value: mobile,
                    dbType: System.Data.DbType.String,
                    direction: System.Data.ParameterDirection.Input
                    );
                _params.Add(
                    name: "@pPassword",
                    value: password,
                    dbType: System.Data.DbType.String,
                    direction: System.Data.ParameterDirection.Input
                    );
                _params.Add(
                    name: "@pVerifyCode",
                    value: verifycode,
                    dbType: System.Data.DbType.String,
                    direction: System.Data.ParameterDirection.Input
                    );
                _params.Add(
                    name: "@pVerifyStatus",
                    value: verifystatus,
                    dbType: System.Data.DbType.String,
                    direction: System.Data.ParameterDirection.Input
                    );
                
                _params.Add(
                    name: "@responseMessage",
                    dbType: System.Data.DbType.String,
                    size: 250,
                    direction: System.Data.ParameterDirection.Output
                    );
                _params.Add(
                   name: "@outUserAccountId",
                   dbType: System.Data.DbType.Guid,
                   direction: System.Data.ParameterDirection.Output
                   );

                var affectedRows = db.Execute(sql, _params, commandType: System.Data.CommandType.StoredProcedure);
                string responseMessage = _params.Get<string>("@responseMessage");
                useraccountid = _params.Get<Guid>("@outUserAccountId");

                if (responseMessage == "Success")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        [Route("api/account/user")]
        [HttpGet]
        [EnableCors("MyPolicy")]
        public JsonResult GetUser()
        {
            using (var db = My.ConnectionFactory())
            {
                return Json(new { users = db.Query<UserAccount>(My.Table_UserAccount.Select).ToList() }, JsonRequestBehavior.AllowGet);
            }
        }

        [Route("api/account/user/{id}")]
        [HttpGet]
        [EnableCors("MyPolicy")]
        public JsonResult GetUserById(string id)
        {
            using (var db = My.ConnectionFactory())
            {
                var user = db.QuerySingle<UserAccount>(My.Table_UserAccount.SelectSingle, new { useraccountid = id });
                return Json(new { user = user }, JsonRequestBehavior.AllowGet);
            }
        }

        //[Authorize]
        [Route("api/account/user")]
        [HttpPost()]
        [EnableCors("MyPolicy")]
        public JsonResult SaveUser(UserAccount userAccount)
        {
            try
            {
                using (var db = My.ConnectionFactory())
                {
                    string sqlQuery = "SELECT * FROM UserAccount "
                         + " WHERE mobile = @mobile";
                    var queryResult = db.QuerySingleOrDefault<UserAccount>(sqlQuery, new { mobile = userAccount.mobile });
                    if (queryResult != null)
                    {
                        return JsonReturn(AppUtilities.JsonStatus.ERROR, "Mobile number already used!");
                    }

                    Guid? outuseraccountid;
                    if (TryRegister(userAccount.username, userAccount.password, userAccount.mobile, CodeGen(5), "wait",out outuseraccountid))
                    {
                        return JsonReturn(AppUtilities.JsonStatus.OK, outuseraccountid.ToString());
                    }
                    else
                    {
                        return JsonReturn(AppUtilities.JsonStatus.ERROR, "Cannot register!");
                    }

                    //string sqlQuery = "SELECT * FROM UserAccount "
                    //     + " WHERE recordstatus<> 2 AND mobile = @mobile";
                    //var queryResult = db.QuerySingleOrDefault<UserAccount>(sqlQuery, new { mobile = userAccount.mobile });
                    //if(queryResult != null)
                    //{
                    //    return JsonReturn(AppUtilities.JsonStatus.ERROR, "Mobile number already used!");
                    //}

                    //userAccount.modifieddate = AppUtilities.DateToString();
                    //userAccount.createddate = AppUtilities.DateToString();
                    //userAccount.verifycode = CodeGen(5);
                    //userAccount.verifystatus = "wait";

                    //int result = db.Execute($@"IF EXISTS({My.Table_UserAccount.SelectSingle}) {My.Table_UserAccount.Update} ELSE {My.Table_UserAccount.Insert}", userAccount);
                }
                
            }
            catch (Exception ex)
            {
                return JsonReturn(AppUtilities.JsonStatus.ERROR, ex.Message);
            }
        }

        [Route("api/account/user/{id}")]
        [HttpDelete()]
        [EnableCors("MyPolicy")]
        public ActionResult Delete(string id)
        {
            try
            {
                using (var db = My.ConnectionFactory())
                {
                    int result = db.Execute(My.Table_UserAccount.Delete, new { useraccountid = id });
                    if (result > 0)
                    {
                        return JsonReturn(AppUtilities.JsonStatus.OK, "Delete successful.");
                    }
                    else
                    {
                        return JsonReturn(AppUtilities.JsonStatus.NOTFOUND, "Record not found!");
                    }
                }
            }
            catch (Exception ex)
            {
                return JsonReturn(AppUtilities.JsonStatus.ERROR, ex.Message);
            }
        }

        [Route("api/account/resendcode")]
        [HttpPost()]
        [EnableCors("MyPolicy")]
        public JsonResult ResendVerifyCode(UserAccount userAccount)
        {
            try
            {
                using (var db = My.ConnectionFactory())
                {
                    userAccount.modifieddate = AppUtilities.DateToString();
                    userAccount.createddate = AppUtilities.DateToString();
                    userAccount.verifycode = CodeGen(5);
                    userAccount.verifystatus = "wait";

                    int result = db.Execute($@"IF EXISTS({My.Table_UserAccount.SelectSingle}) {My.Table_UserAccount.Update} ELSE {My.Table_UserAccount.Insert}", userAccount);
                }
                return JsonReturn(AppUtilities.JsonStatus.OK, userAccount.verifycode);
            }
            catch (Exception ex)
            {
                return JsonReturn(AppUtilities.JsonStatus.ERROR, ex.Message);
            }
        }

        [Route("api/account/verifycode")]
        [HttpPost()]
        [EnableCors("MyPolicy")]
        public JsonResult VerifyCode(UserAccount userAccount)
        {
            try
            {
                using (var db = My.ConnectionFactory())
                {
                    string sqlQuery = "SELECT * FROM UserAccount "
                        + " WHERE "
                        + " useraccountid = @useraccountid";
                    var queryResult = db.QuerySingleOrDefault<UserAccount>(sqlQuery, new { useraccountid = userAccount.useraccountid });
                    if (queryResult != null)
                    {
                        if (queryResult.verifycode != userAccount.verifycode)
                        {
                            return JsonReturn(AppUtilities.JsonStatus.ERROR, "Invalid Code!");
                        }

                        queryResult.verifystatus = "verified";
                        queryResult.verifycode = string.Empty;

                        int result = db.Execute($@"IF EXISTS({My.Table_UserAccount.SelectSingle}) {My.Table_UserAccount.Update} ELSE {My.Table_UserAccount.Insert}", queryResult);
                    }
                }
                return JsonReturn(AppUtilities.JsonStatus.OK, "OK");
            }
            catch (Exception ex)
            {
                return JsonReturn(AppUtilities.JsonStatus.ERROR, ex.Message);
            }
        }

        private ActionResult BadRequest(string message)
        {
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest, message);
        }

        private ActionResult NotFound()
        {
            return new HttpStatusCodeResult(HttpStatusCode.NotFound);
        }

        private ActionResult Ok()
        {
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        private JsonResult JsonReturn(AppUtilities.JsonStatus apistatus, string apitext)
        {

            if (apistatus == AppUtilities.JsonStatus.OK)
            {
                return Json(new { apistatus = "OK", apitext }, JsonRequestBehavior.AllowGet);
            }
            else if (apistatus == AppUtilities.JsonStatus.ERROR)
            {
                return Json(new { apistatus = "ERROR", apitext }, JsonRequestBehavior.AllowGet);
            }
            else if (apistatus == AppUtilities.JsonStatus.NOTFOUND)
            {
                return Json(new { apistatus = "NOTFOUND", apitext }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { apistatus = "UNKNOW", apitext }, JsonRequestBehavior.AllowGet);
            }

        }
    }
}
