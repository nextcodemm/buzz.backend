﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Cors;
using Dapper;
using System.Web.Mvc;
using buzz.backend.Utilities;
using buzz.backend.Models;
using System.Net;
using System.Web;
using System.IO;

namespace buzz.backend.Controllers
{
    public class AppointmentController : Controller
    {
        protected override JsonResult Json(object data, string contentType, System.Text.Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new JsonResult()
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior,
                MaxJsonLength = Int32.MaxValue
            };
        }

        [Route("api/appointmentbydoctorid/{doctorid}")]
        [HttpGet()]
        [EnableCors("MyPolicy")]
        public JsonResult Get(string doctorid)
        {
            using (var db = My.ConnectionFactory())
            {
                string sql = "SELECT A.*, D.doctorid AS Id, D.* FROM Appointment AS A "
                       + " LEFT JOIN Doctor AS D "
                       + " ON A.doctorid = D.doctorid"
                       + " WHERE A.recordstatus<> 2 AND A.doctorid = @doctorid";

                IEnumerable<Appointment> appointments = db.Query<Appointment, Doctor, Appointment>(sql, (iappointment, idoctor) =>
                {
                    iappointment.doctor = idoctor;
                    return iappointment;
                }, new { doctorid = doctorid}, splitOn: "Id").ToList();

                return Json(new { appointments }, JsonRequestBehavior.AllowGet);

            }
        }

        [Route("api/appointmentbyhealthcareunitid/{healthcareunitid}")]
        [HttpGet()]
        [EnableCors("MyPolicy")]
        public JsonResult GetByHealthCareUnit(string healthcareunitid)
        {
            using (var db = My.ConnectionFactory())
            {
                string sql = "SELECT A.*, D.doctorid AS Id, D.* FROM Appointment AS A "
                      + " LEFT JOIN Doctor AS D "
                      + " ON A.doctorid = D.doctorid"
                      + " WHERE A.recordstatus<> 2 AND A.healthcareunitid = @healthcareunitid";

                IEnumerable<Appointment> appointments = db.Query<Appointment, Doctor, Appointment>(sql, (iappointment, idoctor) =>
                {
                    iappointment.doctor = idoctor;
                    return iappointment;
                }, new { healthcareunitid = healthcareunitid }, splitOn: "Id").ToList();

                return Json(new { appointments }, JsonRequestBehavior.AllowGet);

            }
        }

        [Route("api/appointmentbypatientid/{patientid}")]
        [HttpGet()]
        [EnableCors("MyPolicy")]
        public JsonResult GetByPatientid(string patientid)
        {
            using (var db = My.ConnectionFactory())
            {
                string sql = "SELECT A.*, D.doctorid AS Id, D.* FROM Appointment AS A "
                       + " LEFT JOIN Doctor AS D "
                       + " ON A.doctorid = D.doctorid"
                       + " WHERE A.recordstatus<> 2 AND A.patientid = @patientid";

                IEnumerable<Appointment> appointments = db.Query<Appointment, Doctor, Appointment>(sql, (iappointment, idoctor) =>
                {
                    iappointment.doctor = idoctor;
                    return iappointment;
                }, new { patientid = patientid }, splitOn: "Id").ToList();

                return Json(new { appointments }, JsonRequestBehavior.AllowGet);

            }
        }

        [Route("api/appointments/save")]
        [HttpPost]
        [EnableCors("MyPolicy")]
        public ActionResult Save(Appointment appointment)
        {
            try
            {
                using (var db = My.ConnectionFactory())
                {
                    if (appointment.appointmentid == null)
                    {
                        appointment.appointmentid = Guid.NewGuid();
                    }

                    appointment.createddate = (appointment.createddate == null || appointment.createddate.Equals("")) ? AppUtilities.DateToString() : appointment.createddate;
                    appointment.modifieddate = AppUtilities.DateToString();
                    appointment.userid = "thz";
                    appointment.recordstatus = 1;
                    int result = db.Execute($@"IF EXISTS({My.Table_Appointment.SelectSingle}) {My.Table_Appointment.Update} ELSE {My.Table_Appointment.Insert}", appointment);
                }
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Route("api/appointments/appointmentdelete/")]
        [HttpPost()]
        [EnableCors("MyPolicy")]
        public ActionResult Delete(string appointmentid)
        {
            try
            {
                try
                {
                    using (var db = My.ConnectionFactory())
                    {
                        string sql = "Update Appointment Set recordstatus = 2 Where appointmentid = @appointmentid";
                        int result = db.Execute(sql, new { appointmentid = appointmentid });
                    }
                    return Ok();
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        private ActionResult BadRequest(string message)
        {
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest, message);
        }

        private ActionResult NotFound()
        {
            return new HttpStatusCodeResult(HttpStatusCode.NotFound);
        }

        private ActionResult Ok()
        {
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

    }
}