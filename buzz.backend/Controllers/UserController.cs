﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Cors;
using Dapper;
using System.Web.Mvc;
using buzz.backend.Utilities;
using buzz.backend.Models;
using System.Net;
using System.Web;
using System.IO;
using System.Web.Http;

namespace buzz.backend.Controllers
{
    public class UserController : Controller
    {
        protected override JsonResult Json(object data, string contentType, System.Text.Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new JsonResult()
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior,
                MaxJsonLength = Int32.MaxValue
            };
        }

        [Route("api/users/getallusers")]
        [HttpGet]
        [EnableCors("MyPolicy")]
        public JsonResult Get()
        {
            using (var db = My.ConnectionFactory())
            {
                string sql = "SELECT * FROM Users ";

                IEnumerable<Users> users = db.Query<Users>(sql);

                return Json(new { users }, JsonRequestBehavior.AllowGet);
            }
        }

        [Route("api/users/{userid}")]
        [HttpGet()]
        [EnableCors("MyPolicy")]
        public JsonResult Get(string userid)
        {
            using (var db = My.ConnectionFactory())
            {
                string sql = "SELECT * FROM Users WHERE userid = @userid";

                IEnumerable<Users> users = db.Query<Users>(sql, new { userid = userid}).ToList();

                return Json(new { users }, JsonRequestBehavior.AllowGet);

            }
        }

        [Route("api/users/save")]
        [HttpPost]
        [EnableCors("MyPolicy")]
        public ActionResult Save(Users user)
        {
            try
            {
                using (var db = My.ConnectionFactory())
                {
                    if (user.userid == null)
                    {
                        user.userid = Guid.NewGuid();
                    }

                    user.creationdate = (user.creationdate == null || user.creationdate.Equals("")) ? AppUtilities.DateToString() : user.creationdate;
                    int result = db.Execute($@"IF EXISTS({My.Table_Users.SelectSingle}) {My.Table_Users.Update} ELSE {My.Table_Users.Insert}", user);
                }
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Route("api/users/gettotalusers")]
        [HttpGet()]
        [EnableCors("MyPolicy")]
        public JsonResult GetTotalCount()
        {
            using (var db = My.ConnectionFactory())
            {
                string sql = "SELECT COUNT(*) FROM Users";
              
                int count = db.Query<int>(sql).FirstOrDefault();

                return Json(new { count }, JsonRequestBehavior.AllowGet);

            }
        }

        [Route("api/users/{current}/{size}")]
        [HttpGet]
        [EnableCors("MyPolicy")]
        public JsonResult GetByPage(int current, int size)
        {
            using (var db = My.ConnectionFactory())
            {
                string sql = "SELECT  * "
                    + " FROM(SELECT    ROW_NUMBER() OVER(ORDER BY displayname) AS RowNum, * "
                    + "           FROM      Users "
                    + "         ) AS U"
                    + " WHERE RowNum >= @from "
                    + "     AND RowNum< @to "
                    + " ORDER BY RowNum";

                IEnumerable<Users> users = db.Query<Users>(sql, new { from = ((current - 1) * size), to = ((current - 1) * size) + size });

                return Json(new { users }, JsonRequestBehavior.AllowGet);
            }
        }

        [Route("api/users/delete/{userid}")]
        [HttpPost()]
        [EnableCors("MyPolicy")]
        public ActionResult Delete(string userid)
        {
            try
            {
                try
                {
                    using (var db = My.ConnectionFactory())
                    {
                        string sql = "Delete From Users Where userid = @userid";
                        int result = db.Execute(sql, new { userid = userid });
                    }
                    return Ok();
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        private ActionResult BadRequest(string message)
        {
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest, message);
        }

        private ActionResult NotFound()
        {
            return new HttpStatusCodeResult(HttpStatusCode.NotFound);
        }

        private ActionResult Ok()
        {
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }


    }
}