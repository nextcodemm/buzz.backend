﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Cors;
using Dapper;
using buzz.backend.Utilities;
using buzz.backend.Models;
using System.Web.Mvc;
using System.Net;
using System.Data.SqlClient;
using System.Data;

namespace buzz.backend.Controllers
{
    [Route("api/displayboards")]
    public class DisplayBoardController : Controller
    {
        [HttpGet]
        [EnableCors("MyPolicy")]
        public JsonResult Get()
        {
            if(AppUtilities.IsExpired())
            {
                return Json( new { displayBoards = "expired"}, JsonRequestBehavior.AllowGet);
            }
            using (var db = My.ConnectionFactory())
            {
                string query = "SELECT B.*, G.groupid AS Id, G.*, DBS.displayboardid AS Id, DBS.*, D.doctorid AS Id, D.* FROM DisplayBoard AS B "
                     + " INNER JOIN DisplayGroup AS G ON B.groupid = G.groupid "
                     + " LEFT JOIN DisplayBoardSetting AS DBS "
                     + " ON B.displayboardid = DBS.displayboardid "
                     + " LEFT JOIN Doctor AS D "
                     + " ON DBS.doctorid = D.doctorid"
                     + " WHERE B.recordstatus<> 2";

                IEnumerable<DisplayBoard> displayBoards = db.Query<DisplayBoard, DisplayGroup,  DisplayBoardSetting, Doctor, DisplayBoard>(query, (displayBoard, displayGroup, displayboardsetting, doctor) =>
                {
                    displayBoard.DisplayGroup = displayGroup;
                    displayBoard.DisplayBoardSetting = displayboardsetting;
                    displayBoard.Doctor = doctor;
                    return displayBoard;
                }, splitOn: "Id").ToList();
                return Json(new { displayBoards }, JsonRequestBehavior.AllowGet);
            }
        }

        

        [Route("api/displayboards/{id}")]
        [HttpGet()]
        [EnableCors("MyPolicy")]
        public JsonResult Get(string id)
        {
            if (AppUtilities.IsExpired())
            {
                return Json(new { displayBoards = "expired" }, JsonRequestBehavior.AllowGet);
            }
            using (var db = My.ConnectionFactory())
            {
                string query = "SELECT B.*, G.groupid AS Id, G.*, T.templateid AS Id,  T.*, A.adssettingid AS Id, A.*  FROM DisplayBoard AS B INNER JOIN DisplayGroup AS G ON B.groupid = G.groupid "
                    + " LEFT JOIN Template AS T "
                    + " ON B.templateid = T.templateid"
                    + " LEFT JOIN AdsSetting AS A "
                    + " ON B.displayboardid = A.displayboardid "
                    + " WHERE B.recordstatus<> 2 AND B.displayboardid=@displayboardid";
                var displayBoards = db.Query<DisplayBoard, DisplayGroup, Template, AdsSetting, DisplayBoard>(query, (displayBoard, displayGroup, template, adssetting) =>
                {
                    displayBoard.DisplayGroup = displayGroup;
                    displayBoard.Template = template;
                    displayBoard.AdsSetting = adssetting;
                    return displayBoard;
                }, new { displayboardid = id }, splitOn: "Id").ToList();

                if (displayBoards != null && displayBoards.Count > 0) return Json(new { displayBoard = displayBoards[0] }, JsonRequestBehavior.AllowGet);
                else return null;
            }
        }

        [HttpPost]
        [EnableCors("MyPolicy")]
        public ActionResult Save(DisplayBoard board)
        {
            try
            {
                using (var db = My.ConnectionFactory())
                {
                    if (board.displayboardid == null)
                    {
                        board.displayboardid = Guid.NewGuid();
                    }

                    if(board.mainboardid == null)
                    {
                        board.mainboardid = board.displayboardid;
                    }

                    board.createddate = (board.createddate == null || board.createddate.Equals("")) ? AppUtilities.DateToString() : board.createddate;
                    board.modifieddate = AppUtilities.DateToString();
                    if(board.displayorder == 0)
                    {
                        int displayorder = 0;
                        Int32.TryParse("" + db.ExecuteScalar($@"SELECT MAX(displayorder) FROM DisplayBoard WHERE groupid = '" + board.groupid + "' AND mainboardid = '" + board.mainboardid + "'"),out displayorder);
                        board.displayorder = displayorder + 1;
                    }
                    board.userid = "thz";
                    board.recordstatus = 1;
                    int result = db.Execute($@"IF EXISTS({My.Table_DisplayBoard.SelectSingle}) {My.Table_DisplayBoard.Update} ELSE {My.Table_DisplayBoard.Insert}", board);
                }
                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        [Route("api/displayboards/{displayboardid}")]
        [HttpDelete()]
        [EnableCors("MyPolicy")]
        public ActionResult Delete(string displayboardid)
        {
            try
            {
                using (var db = My.ConnectionFactory())
                {
                    int result = db.Execute(My.Table_DisplayBoard.Delete, new { displayboardid = displayboardid });
                    if (result > 0) return Ok(); else return NotFound();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Route("api/displayboards/info")]
        [HttpGet()]
        [EnableCors("MyPolicy")]
        public JsonResult GetDisplayBoard()
        {

            using (var db = My.ConnectionFactory())
            {
                string displayboardsql = "Select  * From displayBoard Where recordstatus<> 2 AND mainboardid = displayboardid";
                string displaygroupsql = "Select * From displaygroup where recordstatus<>2 order by groupname";
                string templatesql = "Select * From template where recordstatus<>2 ";

                IEnumerable<DisplayBoard>displayBoard = db.Query<DisplayBoard>(displayboardsql).ToList();
                IEnumerable<DisplayGroup>  displayGroup = db.Query<DisplayGroup>(displaygroupsql).ToList();
                IEnumerable<Template> template = db.Query<Template>(templatesql).ToList();

                return Json(new { displayBoard, displayGroup, template },JsonRequestBehavior.AllowGet);
            }
        }

        private ActionResult BadRequest(string message)
        {
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest, message);
        }

        private ActionResult NotFound()
        {
            return new HttpStatusCodeResult(HttpStatusCode.NotFound);
        }

        private ActionResult Ok()
        {
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
    }
}
