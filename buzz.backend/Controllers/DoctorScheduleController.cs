﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Cors;
using Dapper;
using System.Web.Mvc;
using buzz.backend.Utilities;
using buzz.backend.Models;
using System.Net;
using System.Web;
using System.IO;

namespace buzz.backend.Controllers
{
    public class DoctorScheduleController : Controller
    {
        protected override JsonResult Json(object data, string contentType, System.Text.Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new JsonResult()
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior,
                MaxJsonLength = Int32.MaxValue
            };
        }

        [HttpGet]
        [EnableCors("MyPolicy")]
        public JsonResult Get()
        {
            using (var db = My.ConnectionFactory())
            {
                string sql = "SELECT DS.*, D.doctorid AS Id, D.* FROM ScheduleDoctor AS DS "
                    + " LEFT JOIN Doctor AS D "
                    + " ON DS.doctorid = D.doctorid"
                    + " WHERE DS.recordstatus<> 2"; 

                IEnumerable<DoctorSchedule> doctorschedules = db.Query<DoctorSchedule, Doctor, DoctorSchedule>(sql, (idoctorschedule, idoctor) =>
                {
                    idoctorschedule.doctor = idoctor;
                    return idoctorschedule;
                }, splitOn: "Id");

                return Json(new { doctorschedules }, JsonRequestBehavior.AllowGet);
            }
        }

        [Route("api/doctorschedulesbydoctorid/{doctorid}")]
        [HttpGet()]
        [EnableCors("MyPolicy")]
        public JsonResult Get(string doctorid,string healthcareunitid)
        {
            using (var db = My.ConnectionFactory())
            {
                string sql = "SELECT DS.*, D.doctorid AS Id, D.* FROM DoctorSchedule AS DS "
                    + " LEFT JOIN Doctor AS D "
                    + " ON DS.doctorid = D.doctorid"
                    + " WHERE DS.recordstatus<> 2 AND D.doctorid = @doctorid "
                    + " AND DS.healthcareunitid = @healthcareunitid";

                IEnumerable<DoctorSchedule> doctorschedules = db.Query<DoctorSchedule, Doctor, DoctorSchedule>(sql, (idoctorschedule, idoctor) =>
                {
                    idoctorschedule.doctor = idoctor;
                    return idoctorschedule;
                }, new { doctorid = doctorid , healthcareunitid = healthcareunitid }, splitOn: "Id").ToList();

                return Json(new { doctorschedules }, JsonRequestBehavior.AllowGet);

            }
        }

        [Route("api/doctorschedules/{doctorscheduleid}")]
        [HttpGet()]
        [EnableCors("MyPolicy")]
        public JsonResult Get(string doctorscheduleid)
        {
            using (var db = My.ConnectionFactory())
            {
                string sql = "SELECT DS.*, D.doctorid AS Id, D.* FROM DoctorSchedule AS DS "
                    + " LEFT JOIN Doctor AS D "
                    + " ON DS.doctorid = D.doctorid"
                    + " WHERE DS.recordstatus<> 2 AND DS.doctorscheduleid = @doctorscheduleid ";

                IEnumerable<DoctorSchedule> doctorschedules = db.Query<DoctorSchedule, Doctor, DoctorSchedule>(sql, (idoctorschedule, idoctor) =>
                {
                    idoctorschedule.doctor = idoctor;
                    return idoctorschedule;
                }, new { doctorscheduleid = doctorscheduleid }, splitOn: "Id").ToList();

                return Json(new { doctorschedules }, JsonRequestBehavior.AllowGet);

            }
        }

        [Route("api/doctorschedules/save")]
        [HttpPost]
        [EnableCors("MyPolicy")]
        public ActionResult Save(DoctorSchedule doctorschedule)
        {
            try
            {
                using (var db = My.ConnectionFactory())
                {
                    if (doctorschedule.doctorscheduleid == null)
                    {
                        doctorschedule.doctorscheduleid = Guid.NewGuid();
                    }

                    doctorschedule.createddate = (doctorschedule.createddate == null || doctorschedule.createddate.Equals("")) ? AppUtilities.DateToString() : doctorschedule.createddate;
                    doctorschedule.modifieddate = AppUtilities.DateToString();
                    doctorschedule.userid = "thz";
                    doctorschedule.recordstatus = 1;
                    int result = db.Execute($@"IF EXISTS({My.Table_DoctorSchedule.SelectSingle}) {My.Table_DoctorSchedule.Update} ELSE {My.Table_DoctorSchedule.Insert}", doctorschedule);
                }
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


        [Route("api/doctorscheducles/doctorscheduledelete/")]
        [HttpPost()]
        [EnableCors("MyPolicy")]
        public ActionResult Delete(string doctorscheduleid)
        {
            try
            {
                try
                {
                    using (var db = My.ConnectionFactory())
                    {
                        string sql = "Update DoctorSchedule Set recordstatus = 2 Where  doctorscheduleid = @doctorscheduleid";
                        int result = db.Execute(sql, new { doctorscheduleid = doctorscheduleid });
                    }
                    return Ok();
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        private ActionResult BadRequest(string message)
        {
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest, message);
        }

        private ActionResult NotFound()
        {
            return new HttpStatusCodeResult(HttpStatusCode.NotFound);
        }

        private ActionResult Ok()
        {
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
    }
}