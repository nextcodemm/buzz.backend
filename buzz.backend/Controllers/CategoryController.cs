﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Cors;
using Dapper;
using System.Web.Mvc;
using buzz.backend.Utilities;
using buzz.backend.Models;
using System.Net;

namespace buzz.backend.Controllers
{
    //[Produces("application/json")]
    [Authorize]
    [Route("api/categories")]
    public class CategoryController : Controller
    {
        [HttpGet]
        [EnableCors("MyPolicy")]
        public JsonResult Get()
        {
            using (var db = My.ConnectionFactory())
            {
                return Json(new { categories = db.Query<Category>(My.Table_Category.Select).ToList() }, JsonRequestBehavior.AllowGet );
            }
        }

        [Route("api/categories/{id}")]
        [HttpGet()]
        [EnableCors("MyPolicy")]
        public JsonResult Get(string id)
        {
            using (var db = My.ConnectionFactory())
            {
                var category = db.QuerySingle<Category>(My.Table_Category.SelectSingle, new { categoryid = id });
                return Json(new { category = category }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [EnableCors("MyPolicy")]
        public ActionResult Save(Category Category)
        {
            try
            {
                using (var db = My.ConnectionFactory())
                {
                    if (Category.categoryid == null)
                    {
                        Category.categoryid = Guid.NewGuid();
                    }
                    Category.createddate = (Category.createddate == null || Category.createddate.Equals("")) ? AppUtilities.DateToString() : Category.createddate;
                    Category.modifieddate = AppUtilities.DateToString();
                    Category.userid = "thz";
                    Category.recordstatus = 1;
                    int result = db.Execute($@"IF EXISTS({My.Table_Category.SelectSingle}) {My.Table_Category.Update} ELSE {My.Table_Category.Insert}", Category);
                }
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Route("api/categories/{categoryid}")]
        [HttpDelete()]
        [EnableCors("MyPolicy")]
        public ActionResult Delete(string categoryid)
        {
            try
            {
                using (var db = My.ConnectionFactory())
                {
                    int result = db.Execute(My.Table_Category.Delete, new { Categoryid = categoryid });
                    if (result > 0) return Ok(); else return NotFound();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        private ActionResult BadRequest(string message)
        {
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest, message);
        }

        private ActionResult NotFound()
        {
            return new HttpStatusCodeResult(HttpStatusCode.NotFound);
        }

        private ActionResult Ok()
        {
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
    }
}
