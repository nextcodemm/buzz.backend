﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Cors;
using Dapper;
using System.Web.Mvc;
using buzz.backend.Utilities;
using buzz.backend.Models;
using System.Net;

namespace buzz.backend.Controllers
{
    //[Produces("application/json")]
    [Route("api/templates")]
    public class TemplateController : Controller
    {
        [HttpGet]
        [EnableCors("MyPolicy")]
        public JsonResult Get()
        {
            using (var db = My.ConnectionFactory())
            {
                return Json(new { templates = db.Query<Template>(My.Table_Template.Select).ToList() }, JsonRequestBehavior.AllowGet);
            }
        }

        [Route("api/templates/{id}")]
        [HttpGet]
        [EnableCors("MyPolicy")]
        public JsonResult Get(string id)
        {
            using (var db = My.ConnectionFactory())
            {
                var template = db.QuerySingle<Template>(My.Table_Template.SelectSingle, new { templateid = id });
                return Json(new { template = template }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [EnableCors("MyPolicy")]
        public ActionResult Save(Template template)
        {
            try
            {
                using (var db = My.ConnectionFactory())
                {
                    if (template.templateid == null)
                    {
                        template.templateid = Guid.NewGuid();
                    }
                    template.createddate = (template.createddate == null || template.createddate.Equals("")) ? AppUtilities.DateToString() : template.createddate;
                    template.modifieddate = AppUtilities.DateToString();
                    template.userid = "thz";
                    template.recordstatus = 1;
                    int result = db.Execute($@"IF EXISTS({My.Table_Template.SelectSingle}) {My.Table_Template.Update} ELSE {My.Table_Template.Insert}", template);
                }
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Route("api/templates/{templateid}")]
        [HttpDelete]
        [EnableCors("MyPolicy")]
        public ActionResult Delete(string templateid)
        {
            try
            {
                using (var db = My.ConnectionFactory())
                {
                    int result = db.Execute(My.Table_Template.Delete, new { templateid = templateid });
                    if (result > 0) return Ok(); else return NotFound();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        private ActionResult BadRequest(string message)
        {
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest, message);
        }

        private ActionResult NotFound()
        {
            return new HttpStatusCodeResult(HttpStatusCode.NotFound);
        }

        private ActionResult Ok()
        {
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
    }
}
