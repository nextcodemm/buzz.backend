﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Cors;
using Dapper;
using System.Web.Mvc;
using buzz.backend.Utilities;
using buzz.backend.Models;
using System.Net;

namespace buzz.backend.Controllers
{
    //[Produces("application/json")]
    [Route("api/queuecounters")]
    public class QueueCounterController : Controller
    {
        [HttpGet]
        [EnableCors("MyPolicy")]
        public JsonResult Get()
        {
            using (var db = My.ConnectionFactory())
            {
                return Json(new { queuecounters = db.Query<QueueCounter>(My.Table_QueueCounter.Select).ToList() }, JsonRequestBehavior.AllowGet);
            }
        }

        [Route("api/queuecounters/{id}")]
        [HttpGet]
        [EnableCors("MyPolicy")]
        public JsonResult Get(string id)
        {
            using (var db = My.ConnectionFactory())
            {
                var queuecounter = db.QuerySingle<QueueCounter>(My.Table_QueueCounter.SelectSingle, new { queuecounterid = id });
                return Json(new { queuecounter = queuecounter }, JsonRequestBehavior.AllowGet);
            }
        }

        [Route("api/queuecountersbycategory/{countercategoryid}")]
        [HttpGet]
        [EnableCors("MyPolicy")]
        public JsonResult GetByCategory(int countercategoryid)
        {
            using (var db = My.ConnectionFactory())
            {
                string sqlQuery = "SELECT * FROM QueueCounter "
                    + " WHERE recordstatus<> 2 AND countercategoryid = @countercategoryid";

                List<QueueCounter> queuecounterlist = db.Query<QueueCounter>(sqlQuery, new { countercategoryid = countercategoryid }).ToList();

                return Json(new { queuecounterlist = queuecounterlist}, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [EnableCors("MyPolicy")]
        public ActionResult Save(QueueCounter queuecounter)
        {
            try
            {
                using (var db = My.ConnectionFactory())
                {
                    if (queuecounter.queuecounterid == null)
                    {
                        queuecounter.queuecounterid = Guid.NewGuid();
                    }
                    if( queuecounter.displayorder == 0)
                    {
                        queuecounter.displayorder = GetMaxDisplayOrder(queuecounter.countercategoryid);
                    }
                    queuecounter.createddate = (queuecounter.createddate == null || queuecounter.createddate.Equals("")) ? AppUtilities.DateToString() : queuecounter.createddate;
                    queuecounter.modifieddate = AppUtilities.DateToString();
                    queuecounter.userid = "thz";
                    queuecounter.recordstatus = 1;
                    int result = db.Execute($@"IF EXISTS({My.Table_QueueCounter.SelectSingle}) {My.Table_QueueCounter.Update} ELSE {My.Table_QueueCounter.Insert}", queuecounter);
                }
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Route("api/queuecountersmaxdisplayorder/{countercategoryid}")]
        [HttpGet]
        [EnableCors("MyPolicy")]
        public int GetMaxDisplayOrder(int? countercategoryid)
        {
            using (var db = My.ConnectionFactory())
            {
                string sqlQuery = "SELECT ISNULL(MAX(displayorder),0) FROM QueueCounter "
                    + " WHERE recordstatus<> 2 AND countercategoryid =  @countercategoryid";

                int maxOrder = db.ExecuteScalar<int>(sqlQuery, new { countercategoryid = countercategoryid });

                return maxOrder + 1;
            }
        }

        [Route("api/queuecounters/{queuecounterid}")]
        [HttpDelete]
        [EnableCors("MyPolicy")]
        public ActionResult Delete(string queuecounterid)
        {
            try
            {
                using (var db = My.ConnectionFactory())
                {
                    int result = db.Execute(My.Table_QueueCounter.Delete, new { queuecounterid = queuecounterid });
                    if (result > 0) return Ok(); else return NotFound();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        private ActionResult BadRequest(string message)
        {
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest, message);
        }

        private ActionResult NotFound()
        {
            return new HttpStatusCodeResult(HttpStatusCode.NotFound);
        }

        private ActionResult Ok()
        {
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
    }
}
